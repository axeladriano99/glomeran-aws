<?php

declare(strict_types=1);

use Illuminate\Support\Facades\Route;
use Stancl\Tenancy\Middleware\InitializeTenancyByDomain;
use Stancl\Tenancy\Middleware\PreventAccessFromCentralDomains;
use App\Http\Controllers\Api\AlmacenController;
use App\Http\Controllers\Api\EmpresaController;
use App\Http\Controllers\Api\ProductController;
use App\Http\Controllers\Api\UserController;
use App\Models\User;
use Illuminate\Http\Request;
use App\Http\Controllers\ValidarController;
use App\Http\Controllers\Api\EmpresFormController;
use App\Http\Controllers\Api\TenantController;
use App\Http\Controllers\CategoriaController;
use App\Http\Controllers\ClienteController;
use App\Http\Controllers\CondicionPagoController;
use App\Http\Controllers\RegistroController;
use App\Http\Controllers\TipoMonedaController;
/*
|--------------------------------------------------------------------------
| Tenant Routes
|--------------------------------------------------------------------------
|
| Here you can register the tenant routes for your application.
| These routes are loaded by the TenantRouteServiceProvider.
|
| Feel free to customize them however you want. Good luck!
|
*/

Route::middleware([
    'web',
    InitializeTenancyByDomain::class,
    PreventAccessFromCentralDomains::class,
])->group(function () {
    // Rutas específicas del inquilino
    Route::get('/tenant/{any}', [TenantController::class, 'index'])->where('any', '.*');

    // Rutas de la aplicación React
    Route::get('/{any}', function () {
        return view('index');
    })->where('any', '.*');
    Route::get('/insertar-registros', [RegistroController::class, 'insertarRegistros']);
    Route::middleware('auth:sanctum')->get('/user', function (Request $request) {
        return $request->user();
    });

    Route::controller(ProductController::class)->group(function(){
        Route::get('/products','index');
        Route::post('/product','store');
        Route::get('/product/{id}','show');
        Route::put('/product/{id}','update');
        Route::delete('/productjj/{id}','destroy');

    });

    Route::controller(UserController::class)->group(function(){
        Route::post('/auth/register','store');
    });
    route::get('/auth/login', [App\Http\Controllers\ValidarController::class, 'store'])->name('/auth/login');
    route::post('/auth/login', [App\Http\Controllers\ValidarController::class, 'store'])->name('/auth/login');
    Route::controller(EmpresFormController::class)->group(function(){
        Route::post('/main','store');
        Route::get('/main','index');
        Route::put('/actualizar','update');
        Route::post('/consulta-ruc','validarRuc');
        Route::get('/consulta-ruc','validarRuc');
        Route::delete('/mainn/{id}','destroy');
    });
    Route::controller(EmpresaController::class)->group(function(){
        Route::get('/empresa','index');
        Route::post('/empresa','store');
        Route::put('/empresa','update');
        Route::delete('/empresa','destroy');
    });
    Route::controller(AlmacenController::class)->group(function(){
        Route::get('/almacen','index');
        Route::post('/almacen','store');
        Route::put('/almacen','update');
        Route::get('/almacen/{id}','show');
        Route::delete('/almacen','destroy');
    });
    Route::controller(ClienteController::class)->group(function(){
        Route::get('/cliente','index');
        Route::post('/cliente','store');
        Route::put('/cliente/{id}','update');
        Route::get('/cliente/{id}','show');
        Route::delete('/cliente/{id}','destroy');
    });
    Route::controller(TipoMonedaController::class)->group(function(){
        Route::get('/moneda','index');
        Route::post('/moneda','store');
        Route::put('/moneda/{id}','update');
        Route::get('/moneda/{id}','show');
        Route::delete('/moneda/{id}','destroy');
    });

    Route::controller(TenantController::class)->group(function(){
        Route::post('/tenant','store');
        Route::get('/tenant','index');
        Route::get('/tenant/{id}','show');
    });
    Route::controller(CategoriaController::class)->group(function(){
        Route::get('/categoria','index');
        Route::post('/categoria','store');
        Route::put('/categoria/{id}','update');
        Route::get('/categoria/{id}','show');
        Route::delete('/categoria/{id}','destroy');
    });

    Route::controller(CategoriaController::class)->group(function(){
        Route::get('/subcategoria','indexSub');
        Route::post('/subcategoria','storeSub');
        Route::put('/subcategoria/{id}','updateSub');
        Route::get('/subcategoria/{id}','showSub');
        Route::delete('/subcategoria/{id}','destroySub');
    });

    Route::controller(CondicionPagoController::class)->group(function(){
        Route::get('/condicionPago','index');
        Route::post('/condicionPago','store');
        Route::put('/condicionPago/{id}','update');
        Route::get('/condicionPago/{id}','show');
        Route::delete('/condicionPago/{id}','destroy');
    });
    });
