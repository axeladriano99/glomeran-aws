<?php

use App\Http\Controllers\Api\AlmacenController;
use App\Http\Controllers\Api\EmpresaController;
use App\Http\Controllers\Api\ProductController;
use App\Http\Controllers\Api\UserController;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\ValidarController;
use App\Http\Controllers\Api\EmpresFormController;
use App\Http\Controllers\Api\TenantController;
use App\Http\Controllers\CategoriaController;
use App\Http\Controllers\ChoferController;
use App\Http\Controllers\ClienteController;
use App\Http\Controllers\CondicionPagoController;
use App\Http\Controllers\Linea_LineaProduccionController;
use App\Http\Controllers\MarcaControlle;
use App\Http\Controllers\PresentacionController;
use App\Http\Controllers\ProductoController;
use App\Http\Controllers\RegistroController;
use App\Http\Controllers\TipoMonedaController;
use App\Http\Controllers\VentasController;

Route::get('/insertar-registros', [RegistroController::class, 'insertarRegistros']);

Route::middleware('auth:sanctum')->get('/user', function (Request $request) {
    return $request->user();
});
Route::controller(AlmacenController::class)->group(function(){
    Route::post('/almacen-dominio','indexDominio');
    Route::post('/almacen','store');
    Route::put('/almacen/{id}','update');
    Route::get('/almacen/{id}','show');
    Route::post('/almacen-delete/{id}','destroy');
});
Route::controller(ClienteController::class)->group(function(){
    Route::post('/cliente-dominio','indexDominio');
    Route::post('/cliente','store');
    Route::put('/cliente/{id}','update');
    Route::get('/cliente/{id}','show');
    Route::post('/cliente-delete/{id}','destroy');
    Route::post('/document','indexDocument');
});
Route::controller(CondicionPagoController::class)->group(function(){
    Route::post('/condicion-dominio','indexDominio');
    Route::post('/condicion','store');
    Route::put('/condicion/{id}','update');
    Route::get('/condicion/{id}','show');
    Route::post('/condicion-delete/{id}','destroy');
});
//alfredo
Route::controller(CategoriaController::class)->group(function(){
    Route::post('/categoria-dominio','index');
    Route::post('/categoria','store');
    Route::put('/categoria/{id}','update');
    Route::get('/categoria/{id}','show');
    Route::post('/categoria-delete/{id}','destroy');
});
//walther
Route::controller(MarcaControlle::class)->group(function(){
    Route::post('/marca-dominio','index');
    Route::post('/marca','store');
    Route::put('/marca/{id}','update');
    Route::get('/marca/{id}','show');
    Route::post('/marca-delete/{id}','destroy');
});
// alfredo
Route::controller(CategoriaController::class)->group(function(){
    Route::post('/subcategoria-dominio','indexSub');
    Route::post('/subcategoria','storeSub');
    Route::put('/subcategoria/{id}','updateSub');
    Route::get('/subcategoria/{id}','showSub');
    Route::post('/subcategoria-delete/{id}','destroySub');
});
Route::controller(TipoMonedaController::class)->group(function(){
    Route::post('/moneda-dominio','index');
    Route::post('/moneda','store');
    Route::put('/moneda/{id}','update');
    Route::get('/moneda/{id}','show');
    Route::post('/moneda-delete/{id}','destroy');
});

Route::controller(TipoMonedaController::class)->group(function(){
    Route::get('/jordana-laboral','indexJornada');
    Route::post('/jordana-laboral','storeJornada');
    Route::put('/jordana-laboral/{id}','updateJornada');
    Route::get('/jordana-laboral/{id}','showJornada');
    Route::delete('/jordana-laboral/{id}','destroyJornada');
});

Route::controller(ProductController::class)->group(function(){
    Route::get('/products','index');
    Route::post('/product','store');
    Route::get('/product/{id}','show');
    Route::put('/product/{id}','update');
    Route::delete('/productjj/{id}','destroy');

});

Route::controller(UserController::class)->group(function(){
    Route::post('/auth/register','store');
});
route::get('/auth/login', [App\Http\Controllers\ValidarController::class, 'store'])->name('/auth/login');
route::post('/auth/login', [App\Http\Controllers\ValidarController::class, 'store'])->name('/auth/login');
Route::controller(EmpresFormController::class)->group(function(){
    Route::post('/main','store');
    Route::get('/main','index');
    Route::put('/actualizar','update');
    Route::post('/consulta-ruc','validarRuc');
    Route::get('/consulta-ruc','validarRuc');
    Route::delete('/mainn/{id}','destroy');
});
Route::controller(EmpresaController::class)->group(function(){
    Route::get('/empresa','index');
    Route::post('/empresa','store');
    Route::put('/empresa','update');
    Route::delete('/empresa','destroy');
});
Route::controller(ChoferController::class)->group(function(){
    Route::get('/chofer','index');
    Route::post('/chofer','store');
    Route::put('/chofer/{id}','update');
    Route::get('/chofer/{id}','show');
    Route::delete('/chofer/{id}','destroy');
});
Route::group(['prefix' => 'moneda'], function () {
    Route::get('/', [TipoMonedaController::class, 'index']);
    Route::post('/', [TipoMonedaController::class, 'store']);
    Route::put('/{id}', [TipoMonedaController::class, 'update']);
    Route::get('/{id}', [TipoMonedaController::class, 'show']);
    Route::delete('/{id}', [TipoMonedaController::class, 'destroy']);
});

Route::controller(TenantController::class)->group(function(){
    Route::post('/tenant','store');
    Route::get('/tenant','index');
    Route::get('/tenant/{id}','show');
});
Route::controller(CondicionPagoController::class)->group(function(){
    Route::get('/condicionPago','index');
    Route::post('/condicionPago','store');
    Route::put('/condicionPago/{id}','update');
    Route::get('/condicionPago/{id}','show');
    Route::delete('/condicionPago/{id}','destroy');
});


Route::controller(Linea_LineaProduccionController::class)->group(function(){
    Route::post('/linea-dominio','index');
    Route::post('/linea','store');
    Route::put('/linea/{id}','update');
    Route::get('/linea/{id}','show');
    Route::delete('/linea/{id}','destroy');
});
Route::controller(Linea_LineaProduccionController::class)->group(function(){
    Route::post('/lineaProduccion-dominio','indexProduccion');
    Route::post('/lineaProduccion','storeProduccion');
    Route::put('/lineaProduccion/{id}','updateProduccion');
    Route::get('/lineaProduccion/{id}','showProduccion');
    Route::post('/lineaProduccion-delete/{id}','destroyProduccion');
});

Route::controller(VentasController::class)->group(function(){
    Route::post('/ventas-dominio','index');
    Route::post('/ventas','store');
    Route::put('/ventas/{id}','update');
    Route::get('/ventas/{id}','show');
    Route::post('/ventas-delete/{id}','destroy');
});

Route::controller(ProductoController::class)->group(function(){
    Route::post('/producto-dominio','index');
    Route::post('/producto','store');
    Route::put('/producto/{id}','update');
    Route::get('/producto/{id}','show');
    Route::post('/producto-delete/{id}','destroy');
});

Route::controller(PresentacionController::class)->group(function(){
    Route::post('/presentacion-dominio','index');
});
