<?php

use Illuminate\Support\Facades\Route;

use App\Http\Controllers\RegistroController;
use Stancl\Tenancy\Tenancy;
use App\Models\Tenant;

Route::get('/{any}', function () {
    return view('index');
})->where('any', '.*');

Route::get('/insertar-registros', [RegistroController::class, 'insertarRegistros']);

