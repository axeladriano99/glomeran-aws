<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Registro extends Model
{
    use HasFactory;

    protected $fillable = [
        'idregistro',
        'nombres_apellidos',
        'razon_social',
        'gmail',
        'telefono',
        'ruc',
        'password',
    ];
}
