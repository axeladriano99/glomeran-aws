<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Empresa extends Model
{
    use HasFactory;
    protected $fillable = [
        'emp_ruc',
        'emp_nom',
        'emp_nomcom',
        'emp_repleg',
        'emp_dir',
        'ubi_id',
        'emp_ema',
        'emp_tel',
        'emp_cel',
        'emp_ctaaho',
        'emp_ctaint',
        'emp_lat',
        'emp_lon',
        'emp_est',
        'emp_per',
        'emp_ret',
        'emp_det',
        'emp_igv',
        'fe_cerrut',
        'fe_cercla',
        'fe_car',
        'fe_log',
        'fe_snttip',
        'fe_sntruc',
        'fe_sntusu',
        'fe_sntcla',
        'fe_porweb',
        'fe_porftp',
        'fe_porusu',
        'fe_porcla',
    ];
}
