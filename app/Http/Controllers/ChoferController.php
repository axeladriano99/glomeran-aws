<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
class ChoferController extends Controller
{
    /**
     * Display a listing of the resource.
     */
    public function index()
    {
        $chofer = DB::Table('chofer')->where('ch_est',1)->get();
        return $chofer;
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(Request $request)
    {
        $chofer = DB::Table('chofer')->insert([
            'idEmpresa' => $request->idempresa,
            'ch_cod' => $request->cod,
            'ane_id' => $request->anexo,
            'per_id' => $request->per_id,
            'id_doc' => $request->id_doc,
            'ch_numdoc'=>$request->ch_numdoc,
            'ch_razsoc' => $request->ch_razsoc,
            'ch_nom'=> $request->ch_nom,
            'ch_apepat' => $request->ch_apepat,
            'ch_bre' => $request->ch_bre,
            'ch_est'=>1,
        ]);
        return response()->json(['message' => 'Chofer creado correctamente'], 200);
    }

    /**
     * Display the specified resource.
     */
    public function show(string $id)
    {
        $chofer = DB::Table('chofer')->where('ch_id',$id)->where('ch_est',1)->first();
        return $chofer;
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(Request $request, string $id)
    {
        $chofer = DB::Table('chofer')->where('ch_id',$id)->update([
            'idEmpresa' => $request->idempresa,
            'ch_cod' => $request->cod,
            'ane_id' => $request->anexo,
            'per_id' => $request->per_id,
            'id_doc' => $request->id_doc,
            'ch_numdoc'=>$request->ch_numdoc,
            'ch_razsoc' => $request->ch_razsoc,
            'ch_nom'=> $request->ch_nom,
            'ch_apepat' => $request->ch_apepat,
            'ch_bre' => $request->ch_bre,
            'ch_est'=>1,
        ]);
        return response()->json(['message' => 'Chofer actualizada correctamente'], 200);
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy(string $id)
    {
        $chofer = DB::Table('chofer')->where('ch_id',$id)->update([
            'ch_est'=>0,
        ]);
        return response()->json(['message' => 'Chofer eliminado correctamente'], 200);
    }
}
