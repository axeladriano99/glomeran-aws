<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Models\Tenant;
use Stancl\Tenancy\Tenancy;
class CondicionPagoController extends Controller
{
    public function Dominio($bd)
    {
        $tenant = Tenant::where('id', $bd)->first();
        $tenancy = tenancy()->initialize($tenant);
        return $tenancy;

    }
    public function index(Request $request)
    {
        $i = $request->json('inquilino');
        $tenancy = $this->Dominio($i);
        $condicion = DB::Table('condicion_pago')->where('pago_estado',1)->get();
        return $condicion;
    }
    public function store(Request $request)
    {
        $i = $request->json('inquilino');
        $tenancy = $this->Dominio($i);
        $condicion = DB::Table('condicion_pago')->where('pago_estado',1)->insert([
            'pago_nom' => $request->pago_nom,
            'pago_estado'=> 1,
        ]);
        return response()->json(['menssage' => 'Categoria Creada correctamente']);
    }
    public function update(Request $request, string $id)
    {
        $i = $request->json('inquilino');
        $tenancy = $this->Dominio($i);
        $condicion = DB::Table('condicion_pago')->where('pago_estado',1)->update([
            'pago_nom' => $request->pago_nom,
        ]);
        return response()->json(['menssage' => 'Categoria actualizada correctamente']);
    }
    public function destroy(Request $request,string $id)
    {   $i = $request->json('inquilino');
        $tenancy = $this->Dominio($i);
        $condicion = DB::Table('condicion_pago')->where('pago_estado',1)->update([
            'pago_estado'=> 0,
        ]);
        return response()->json(['menssage' => 'Categoria actualizada correctamente']);
    }
}
