<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Models\Tenant;
use Stancl\Tenancy\Tenancy;
class CategoriaController extends Controller
{
    public function Dominio($bd)
    {
        $tenant = Tenant::where('id', $bd)->first();
        $tenancy = tenancy()->initialize($tenant);
        return $tenancy;

    }
    public function index(Request $request)
    {
        $i = $request->json('inquilino');
        $tenancy = $this->Dominio($i);
        $categoria = DB::Table('categoria')->where('categ_estado',1)->get();
        tenancy()->end();
        return $categoria;
    }

    public function store(Request $request)
    {
        $i = $request->json('inquilino');
        $tenancy = $this->Dominio($i);
        $categoria = DB::Table('categoria')->insert([
            'nombre' => $request->nombre,
            'codigo_categoria' => $request->codigo_categoria,
            'categ_estado' => 1,
            'created_at'=> now(),
        ]);
        tenancy()->end();
        return response()->json(['message' => 'Categoria creada correctamente'], 200);
    }
    /*
    public function show(string $id)
    {
        $inquilino = $request->json('inquilino');
        $tenant = Tenant::where('id', $inquilino)->first();
        tenancy()->initialize($tenant);
        $categoria = DB::Table('categoria')->where('categ_estado',1)->where('id_categoria',$id)->get();
        return $categoria;
    }
    */
    /**
     * Update the specified resource in storage.
     */
    public function update(Request $request, string $id)
    {
        $i = $request->json('inquilino');
        $tenancy = $this->Dominio($i);
        $categoria = DB::Table('categoria')->where('id_categoria',$id)->update([
            'nombre' => $request->nombre,
            'codigo_categoria'=> $request->codigo_categoria,
            'updated_at' => now(),
        ]);
        tenancy()->end();
        return response()->json(['message' => 'Categoria actualizada correctamente'], 200);
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy(Request $request,string $id)
    {
        $i = $request->json('inquilino');
        $tenancy = $this->Dominio($i);
        $categoria = DB::Table('categoria')->where('id_categoria',$id)->update([
            'categ_estado' => 0,
        ]);
        tenancy()->end();
        if($categoria){
            return response()->json(['message' => 'Categoria eliminada correctamente'], 200);
        }
        else{
            return response()->json(['message' => 'No se pudo eliminar'], 200);
        }

    }
    //subcategora
    public function indexSub(Request $request){
        $i = $request->json('inquilino');
        $tenancy = $this->Dominio($i);
        $subcategoria = DB::Table('subcategoria as sub')
        ->select('sub.id_Subcateg','sub.codigo_subcateg','sub.id_categoria','sub.nombre','sub.sub_abre','categ.nombre as nombre_categoria')
        ->join('categoria as categ','categ.id_categoria','=','sub.id_categoria')
        ->where('sub.subCat_estado', 1)->get();
        tenancy()->end();
        return $subcategoria;
    }
    public function storeSub(Request $request)
    {
        $i = $request->json('inquilino');
        $tenancy = $this->Dominio($i);
        $subcategoriaId = DB::table('subcategoria')->insert([
            'codigo_subcateg' => $request->codigo_subcateg,
            'id_categoria' => $request->id_categoria,
            'nombre' => $request->nombre,
            'sub_abre' => $request->sub_abre,
            'subCat_estado' => '1',
            'created_at' => now(),
        ]);
        tenancy()->end();
        return response()->json(['message' => 'Sub categoria creada correctamente'], 200);
    }
    public function updateSub(Request $request,string $id)
    {
        $i = $request->json('inquilino');
        $tenancy = $this->Dominio($i);
        $subcategoria = DB::Table('subcategoria')->where('id_Subcateg',$id)->update([
            'codigo_subcateg' => $request->codigo_subcateg,
            'id_categoria' => $request->id_categoria,
            'nombre' => $request->nombre,
            'sub_abre' => $request->sub_abre,
            'subCat_estado' => '1',
            'updated_at'=> now(),
        ]);
        tenancy()->end();
        return response()->json(['message' => 'Sub categoria actualizada correctamente'], 200);
    }
    public function destroySub(Request $request, string $id)
    {
        $i = $request->json('inquilino');
        $tenancy = $this->Dominio($i);
        $subcategoria = DB::Table('subcategoria')->where('id_Subcateg',$id)->update([
            'subCat_estado' => 0,
        ]);
        tenancy()->end();
        return response()->json(['message' => 'SubCategoria eliminada correctamente'], 200);
    }
}
