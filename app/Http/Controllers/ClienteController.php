<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Models\Tenant;
use Stancl\Tenancy\Tenancy;
class ClienteController extends Controller
{
    public function Dominio($bd)
    {
        $tenant = Tenant::where('id', $bd)->first();
        $tenancy = tenancy()->initialize($tenant);
        return $tenancy;

    }
    public function indexDominio(Request $request)
    {
        $inquilino = $request->json('inquilino');
        $tenant = $this->Dominio($inquilino);
        $cliente = DB::Table('anexo')->select('ane_id','ane_tipdoc','ane_numdoc','ane_nom',
        'ane_dir','ubi_id','ane_tel','ane_ema','document.doc_nomb')
        ->join('document','document.id','=','anexo.ane_tipdoc')
        ->where('ane_est',1)->where('anexo.cliente',1)->get();
        tenancy()->end();
        return $cliente;
    }

    public function store(Request $request)
    {
        $inquilino = $request->json('inquilino');
        $tenant = Tenant::where('id', $inquilino)->first();
        tenancy()->initialize($tenant);
        $cliente = DB::Table('anexo')->insert([
            'ane_tipdoc' => $request->ane_tipdoc,
            'ane_numdoc'=> $request->ane_numdoc,
            'ane_nom' => $request->ane_nom,
            'ane_dir'=> $request->ane_dir,
            'ubi_id'=> $request->ubi_id,
            'ane_tel' => $request->ane_tel,
            'ane_ema' => $request->ane_ema,
            'cliente' => 1,
            'ane_est' => 1,
            'created_at'=>now(),
            'updated_at' => now(),
        ]);
        tenancy()->end();
        return response()->json(['message' => 'Agregado a la bd :D '], 201);
    }

    public function update(Request $request, string $id)
    {
        $inquilino = $request->json('inquilino');
        $tenant = Tenant::where('id', $inquilino)->first();
        tenancy()->initialize($tenant);
        $cliente = DB::Table('anexo')->where('ane_id',$id)->update([
            'ane_tipdoc' => $request->ane_tipdoc,
            'ane_numdoc'=> $request->ane_numdoc,
            'ane_nom' => $request->ane_nom,
            'ane_dir'=> $request->ane_dir,
            'ubi_id'=> $request->ubi_id,
            'ane_tel' => $request->ane_tel,
            'ane_ema' => $request->ane_ema,
            'cliente' => 1,
            'ane_est' => 1,
            'created_at'=>now(),
            'updated_at' => now(),
        ]);
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy(Request $request, string $id)
    {
        $inquilino = $request->json('inquilino');
        $tenant = Tenant::where('id', $inquilino)->first();
        tenancy()->initialize($tenant);
        $cliente = DB::Table('anexo')->where('ane_id',$id)->update([
            'ane_est' => 0,
            'cliente'=> 0,
        ]);
        tenancy()->end();

    }

    public function indexDocument (Request $request){
        $inquilino = $request->json('inquilino');
        $tenant = $this->Dominio($inquilino);
        $document = DB::table('document')->get();
        tenancy()->end();
        return $document;
    }
}
