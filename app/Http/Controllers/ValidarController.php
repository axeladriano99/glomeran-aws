<?php

namespace App\Http\Controllers;


use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Http\Request;
use Firebase\JWT\JWT;
use DateTime;

class ValidarController extends Controller
{

public function store(Request $request)
{
    $credentials = [
        'email' => $request->input('email'),
        'password' => $request->input('password'),
    ];

    $user = DB::table('users')->where('email', $request->input('email'))->first();


    if ($request->input('email') == $user->email && $user->idEstado === '2') {
        $error = "Usuario inactivo";
        return response()->json(['error' => $error], 401);
    }
    elseif ($request->input('password') == $user->password) {
        Auth::loginUsingId($user->id);
        $vencimiento = now()->addHour(1);
        $token = $user->createToken("API TOKEN", ['*'], new DateTime($vencimiento));
        //return response()->json(['user' => 'walther', 'accessToken' => 'aufY6CJ6i3f0B9KGVmuP6id4VrIF0'], 200);
        return response()->json([
            'status' => true,
            'message' => $request->name(),
            'user' => $user,
            'accessToken' => $token->plainTextToken
        ], 200);
    } else {
        return response()->json(['error' => 'Credenciales inválidas'], 401);
    }
}
    public function Empresa(Request $request) {
        /*$products = DB::table('company_form')->select('*')->get();
        return $products;*/
        $insercion = DB::table('company_form')->select('*')->insert([
            'name' => $request->input('nombre'),
            'email' => $request->input('correo'),
            'telefono' => $request->input('telefono'),
            'ruc' => $request->input('ruc'),
            'password' => $request->input('password'),
        ]);


        // Verificación de éxito de la inserción
        if ($insercion) {
            return response()->json(['message' => 'Usuario creado correctamente'], 200);
        } else {
            return response()->json(['error' => 'Error al crear el usuario'], 500);
        }
    }



}
