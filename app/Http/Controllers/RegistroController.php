<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Registro;
use Illuminate\Support\Facades\DB;
class RegistroController extends Controller
{
    public function insertarRegistros()
    {
        $totalRegistros = 5000;
        $registrosPorLote = 5000;

        ini_set('memory_limit', '1024M');
        set_time_limit(0);

        // Iniciar una transacción
        DB::beginTransaction();

        try {
            for ($i = 0; $i < $totalRegistros; $i += $registrosPorLote) {
                Registro::factory()->count($registrosPorLote)->create();
            }

            // Confirmar la transacción
            DB::commit();
        } catch (\Exception $e) {
            DB::rollBack();
            return "Error al insertar registros: " . $e->getMessage();
        }

        return "Registros insertados correctamente.";
    }


}
