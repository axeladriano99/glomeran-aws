<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
class ProductoController extends Controller
{
    /**
     * Display a listing of the resource.
     */
    public function index()
    {
        $producto = DB::table('producto')->select('idPro',);

    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(Request $request)
    {
        $producto = DB::Table('producto')->insert([
            'idAlmacen' => $request->idAlmacen,
            'idPre'=> $request->idPre,
            'pro_fac'=> 1,
            'pro_nom'=> $request->pro_nom,
            'pro_catego'=>$request->pro_categoria,
            'scat_id'=>$request->scat_id,
            'pro_igv'=>$request->pro_igv,
            'pro_isc'=> $request->pro_isc,
            'pro_est'=>1,


        ]);

    }

    /**
     * Display the specified resource.
     */
    public function show(string $id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(Request $request, string $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy(string $id)
    {
        //
    }
}
