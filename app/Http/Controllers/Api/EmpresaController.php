<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Http;
use Illuminate\Support\Facades\DB;

class EmpresaController extends Controller
{
    public function index(){
        return DB::select('CALL spMostrarEmpresa()');
    }
}
