<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Tenant;
use Stancl\Tenancy\Tenancy;
use Illuminate\Support\Facades\DB;


class TenantController extends Controller
{
    public function index()
{
    // Suponiendo que 'api' es el valor del campo 'id' en tu tabla de inquilinos
    $tenant = Tenant::where('id', 'api')->first();

    if ($tenant) {
        // Inicializar el inquilino actual
        tenancy()->initialize($tenant);

        // Obtener todos los inquilinos
        $tenants = DB::Table('registros')->select('*')->get();
        tenancy()->end();
        return $tenants;
    } else {
        return response()->json(['error' => 'Inquilino no encontrado'], 404);
    }
}

    public function create(){

    }
    public function store(Request $request)
    {
        try {
            // Asumiendo que el nombre del tenant está en el campo 'id' del JSON
            $tenantName = $request->json('id');
            $idregistro = $request->json('idregistro');
            // Verificar que el nombre del inquilino no sea nulo
            $idEmp= DB::Table('registros')->where('idregistro',$idregistro)->first();

            if ($tenantName) {
                // Crear el Tenant con el nombre obtenido
                $tenant = Tenant::create($request->json()->all());
                $tenant->domains()->create([
                    'domain' => $tenantName.'.'.'glomeran.com',
                    'emp' => $idregistro,
                ]);
                $tenant = Tenant::first();
                tenancy()->initialize($tenant);
                $insert = DB::Table('anexo')->insert([
                    'ane_tipdoc' => 2,
                    'ane_razsoc'=>$idEmp->razon_social,
                    'ane_numdoc'=> $idEmp->ruc,
                    'ane_nom' => $idEmp->nombres_apellidos,
                    'ane_tel' => $idEmp->telefono,
                    'ane_ema' => $idEmp->gmail,
                    'ane_est' => 1,
                    'created_at'=>now(),
                    'updated_at' => now(),
                ]);
                tenancy()->end();
                return response()->json(['message' => 'fsajbfsabjf'], 201);


            } else {
                // Devolver un error si el nombre del inquilino es nulo
                return response()->json(['error' => 'Nombre del inquilino nulo'], 400);
            }
        } catch (\Exception $e) {
            // Capturar la excepción y mostrar detalles
            return response()->json(['error' => 'Error al crear el inquilino', 'details' => $e->getMessage()], 500);
        }
    }
    public function show($id)
    {

    }
    public function update(Request $request, string $id)
    {

    }
    public function destroy(string $id)
    {

    }
}
