<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Crypt;
use App\Models\User;
use DateTime;
class UserController extends Controller
{
    public function index()
    {
        $products = User::all();
        return $products;

        //
    }
    public function store(Request $request)
    {
        //incriptar contraseña
        $user = User::create([
            'name' => $request->input('name'),
            'email' => $request->input('email'),
            'password' => $request->input('password'),
        ]);
        $vencimiento = now()->addHour(1);
        $token = $user->createToken("API TOKEN", ['*'], new DateTime($vencimiento));

        return response()->json([
            'status' => true,
            'message' => $request->user(),
            'user' => $user,
            'accessToken' => $token->plainTextToken
        ], 200);

    }

    public function show($id)
    {
        $product = User::find($id);
        return $product;
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(Request $request, string $id)
    {
        $product = User::findOrFail($request->id);
        $product->descripcion = $request->descripcion;
        $product->price= $request->price;
        $product->stock = $request->stock;
        $product->save();
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy(string $id)
    {
        $product = User::destroy($id);
        return $product;
    }
}
