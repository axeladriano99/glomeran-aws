<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Http;
use Illuminate\Support\Facades\DB;
use App\Models\Tenant;
use Stancl\Tenancy\Tenancy;
use GuzzleHttp\Exception\ClientException;
class AlmacenController extends Controller
{
    public function Dominio($bd)
    {
        $tenant = Tenant::where('id', $bd)->first();
        $tenancy = tenancy()->initialize($tenant);
        return $tenancy;
    }
    public function indexDominio(Request $request)
    {
        $inquilino = $request->json('inquilino');
        $tenant = $this->Dominio($inquilino);
        $almacen = DB::select('CALL spMostrarAlmacen()');
        tenancy()->end();
        return $almacen;
    }

    public function store(Request $request)
    {
        $inquilino = $request->json('inquilino');
        $tenant = $this->Dominio($inquilino);
        $insercion = DB::table('almacen')->insertGetId([
            'idAlmacen' => $request['idAlmacen'],
            'id_empresa' => $request['id_empresa'],
            'alm_nomb' => $request['alm_nomb'],
            'direccion' => $request['direccion'],
            'idEstado' => 1,
            'created_at' => now(),
            'updated_at' => now(),
        ]);
        tenancy()->end();
        if($insercion)
            return response()->json(['message' => 'Almacen creado correctamente'], 200);
        else
            return response()->json(['message' => 'Incorrecto'], 200);

    }
    public function show($id){
        $almacen = DB::table('almacen')
            ->join('empresa', 'empresa.idEmpresa', '=', 'almacen.id_empresa')
            ->select('empresa.idEmpresa','empresa.emp_nom','almacen.*')
            ->where('idAlmacen', '=', $id)
            ->first();
        return $almacen;
    }


    public function update(Request $request,string $id)
    {
        $inquilino = $request->json('inquilino');
        $tenant = $this->Dominio($inquilino);

        try {
            $nombre = $request->alm_nomb;
            $direccion = $request->direccion;

            // Adjust the order and data types of parameters based on your stored procedure
            DB::table('almacen')->where('idAlmacen',$id)->update([
                'alm_nomb'=> $nombre,
                'direccion' => $direccion,
            ]);

            return response()->json(['message' => 'Procedimiento almacenado ejecutado con éxito'], 200);

        } catch (\Exception $e) {
            // Log or provide more detailed error information
            return response()->json(['error' => 'Error al actualizar el registro', 'details' => $e->getMessage()], 500);
        }
    }


    /**
     * Remove the specified resource from storage.
     */
    public function destroy(Request $request, string $id)
    {
        $inquilino = $request->json('inquilino');
        $tenant = $this->Dominio($inquilino);
        $almacen = DB::table('almacen')->where('idAlmacen',$id)->update([
            'idEstado' => 0
        ]);
        tenancy()->end();
        return response()->json(['message' => 'Almacen eliminado'], 200);
    }
}
