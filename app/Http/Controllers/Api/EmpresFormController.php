<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Product;
use GuzzleHttp\Exception\ClientException;
use Illuminate\Support\Facades\Http;
use Illuminate\Support\Facades\DB;
use App\Models\Registro;
use Illuminate\Support\Facades\Cache;
class EmpresFormController extends Controller
{

    public function index()
    {
        return DB::select('CALL spMostrarEmpresaForm()');
    }
    /*

    public function index()
    {
        $products = DB::table('registro')->select('*')->get();
        return $products;
    }*/
    public function store(Request $request) {
        /*$products = DB::table('company_form')->select('*')->get();
        return $products;

        $product = new Product();
        $product->descripcion = $request->descripcion;
        $product->price= $request->price;
        $product->stock = $request->stock;
        $product->save();
        */
       // insertar un json es asi
       $validar = DB::table('registros')->where('ruc', $request['ruc'])->first();


       if ($validar)
        {
        return response()->json(['message' => 'Error: algunos datos ya fueron ingresados'],404);
        }
        else {
            $insercion = DB::table('registros')->insertGetId([
                'nombres_apellidos' => $request['nombres'],
                'razon_social' => $request['razon_social'],
                'gmail' => $request['gmail'],
                'telefono' => $request['telefono'],
                'ruc' => $request['ruc'],
                'password' => bcrypt($request['password']),
                'created_at' => now(),
                'updated_at' => now(),
            ]);

            if ($insercion) {
                return response()->json(['message' => 'Usuario creado correctamente'], 200);
            } else {
                return response()->json(['error' => 'Error al crear el usuario'], 500);
            }
        }
    }
    public function update(Request $request)
{
    try{
    $id = $request->idregistro;

    if ($id <= 0) {
        return response()->json(['message' => 'Registro no es válido'], 400);
    }

    $nombres_apellidos = $request->nombres_apellidos;
    $gmail = $request->gmail;
    $telefono = $request->telefono;

    $minutes = 60;

      // Llamada al procedimiento almacenado
        DB::statement("CALL spUpdateRegistro(?, ?, ?, ?)", [
            $id,
            $nombres_apellidos,
            $gmail,
            $telefono,
        ]);
        return response()->json(['message' => 'Registro actualizado correctamente'], 200);

}
    catch (\Exception $e) {
return response()->json(['error' => 'Error al actualizar el registro', 'details' => $e->getMessage()], 500);
    }
}





    public function validarRuc(Request $request)
    {
        $datos = [
            'token' => 'c386cde7-9caf-40b0-a7aa-73b53b2898aa-c4bd3f3f-43a1-44eb-a8c2-86e77c9269cc',
            'ruc' => $request['ruc'],
        ];

        $api = "https://ruc.com.pe/api/v1/consultas";


        try {
            // Realizar la solicitud a la API utilizando GuzzleHTTP
            $response = Http::post($api, $datos);


                $respuestaApi = $response->json();
                return   $responseBody = json_decode($response->body(), true);

        } catch (ClientException $e) {
            // Manejar errores específicos de cliente (por ejemplo, errores 4xx)
            return response()->json(['error' => 'Error en la solicitud a la API'], $e->getCode());
        } catch (\Exception $e) {
            // Manejar otros errores generales
            return response()->json(['error' => 'Ocurrió un error al consumir la API'], 404);
        }
    }
    public function destroy(string $id)
{
    $affectedRows = DB::table('registros')->where('idregistro', $id)->delete();
    return $affectedRows;
}



}
