<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Models\Tenant;
use Stancl\Tenancy\Tenancy;
class MarcaControlle extends Controller
{
    public function Dominio($bd)
    {
        $tenant = Tenant::where('id', $bd)->first();
        $tenancy = tenancy()->initialize($tenant);
        return $tenancy;

    }
    public function index(Request $request)
    {
        $i = $request->json('inquilino');
        $tenancy = $this->Dominio($i);
        $marca = DB::Table('marca')->where('marca_estado',1)->get();
        tenancy()->end();
        return $marca;
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(Request $request)
    {
        $i = $request->json('inquilino');
        $tenancy = $this->Dominio($i);
        $marca = DB::Table('marca')->insert([
            'marca_nombre' =>$request->marca_nombre,
            'marca_estado'=> 1,
        ]);
        tenancy()->end();
        return response()->json(['message' => 'Marca creada correctamente'], 200);
    }

    /**
     * Display the specified resource.
     */
    public function show(string $id)
    {
        $marca = DB::Table('marca')->where('idmarca',$id)->where('marca_estado',1)->get();
        return $marca;
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(Request $request, string $id)
    {
        $i = $request->json('inquilino');
        $tenancy = $this->Dominio($i);
        $marca = DB::Table('marca')->where('idmarca',$id)->update([
            'marca_nombre' =>$request->marca_nombre
        ]);
        return response()->json(['message' => 'Marca creada correctamente'], 200);
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy(Request $request,string $id)
    {
        $i = $request->json('inquilino');
        $tenancy = $this->Dominio($i);
        $marca = DB::Table('marca')->where('idmarca',$id)->update([
            'marca_estado'=> 0,
        ]);
        return response()->json(['message' => 'Marca elimanada correctamente'], 200);
    }
}
