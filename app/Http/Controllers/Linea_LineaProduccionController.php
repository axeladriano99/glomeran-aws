<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\DB;
use App\Models\Tenant;
use Stancl\Tenancy\Tenancy;
class Linea_LineaProduccionController extends Controller
{
    public function index(Request $request)
    {
        $inquilino = $request->json('inquilino');
        $tenant = Tenant::where('id', $inquilino)->first();
        tenancy()->initialize($tenant);
        return DB::select('CALL spMostrarLinea()');
       /*
       $lineas = Cache::remember('lineas_cache', now()->addMinute(1), function () {
        // Si no están en caché, ejecuta la consulta y almacena los resultados en caché
        return DB::select('CALL spMostrarLinea()');
        });


        return $lineas;*/

    /*
    return $lineas;
       return DB::select('CALL spMostrarLinea()');*/
    }
    public function store(Request $request)
    {
        $linea = DB::table('linea')->insert([
            'idEmpresa' => $request->empresa,
            'lin_cod' => $request->codigo,
            'lin_nom' => $request->nombre,
            'lin_abr' => $request->abreviatura,
            'lin_est' => 1,
        ]);
        return response()->json(['mensage'=> $linea]);
    }
    public function show(string $id)
    {
        $linea = DB::Table('linea')->where('lin_id',$id)->first();
        return $linea;
    }
    public function update(Request $request, string $id)
    {
        $linea = DB::table('linea')->where('lin_id',$id)->update([
            'idEmpresa' => $request->empresa,
            'lin_cod' => $request->codigo,
            'lin_nom' => $request->nombre,
            'lin_abr' => $request->abreviatura,
        ]);
    }
    public function destroy(string $id)
    {
        $linea = DB::table('linea')->where('lin_id',$id)->update([
            'lin_est' => 0,
        ]);
        return response()->json(['mensage'=> $linea]);
    }

    public function indexProduccion()
    {

        return DB::select('CALL spMostrarLineaProduccion()');
       /*
       $lineas = Cache::remember('lineas_cache', now()->addMinute(1), function () {
        // Si no están en caché, ejecuta la consulta y almacena los resultados en caché
        return DB::select('CALL spMostrarLinea()');
        });


        return $lineas;*/

    /*
    return $lineas;
       return DB::select('CALL spMostrarLinea()');*/
    }
    public function storeProduccion(Request $request)
    {
        $linea = DB::table('linea_produccion')->insert([
            'linpro_nom' => $request->nombre,
            'linpro_est' => 1,
        ]);
        return response()->json(['mensage'=> $linea]);
    }
    public function showProduccion(string $id)
    {
        $linea = DB::table('linea_produccion')->where('linpro_id',$id)->first();
        return $linea;
    }
    public function updateProduccion(Request $request, string $id)
    {
        $linea = DB::table('linea')->where('linpro_id',$id)->update([
            'linpro_nom' => $request->nombre,
            'linpro_est' => 1,
        ]);
        return response()->json(['mensage'=> $linea]);
    }
    public function destroyProduccion(string $id)
    {
        $linea = DB::table('linea_produccion')->where('linpro_id',$id)->update([
            'linpro_est' => 0,
        ]);
        return response()->json(['mensage'=> $linea]);
    }
}
