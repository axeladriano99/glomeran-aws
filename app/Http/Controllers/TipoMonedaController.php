<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Models\Tenant;
use Stancl\Tenancy\Tenancy;
class TipoMonedaController extends Controller
{
    public function Dominio($bd)
    {
        $tenant = Tenant::where('id', $bd)->first();
        $tenancy = tenancy()->initialize($tenant);
        return $tenancy;

    }
    public function index(Request $request)
    {
        $i = $request->json('inquilino');
        $tenancy = $this->Dominio($i);
        $moneda = DB::Table('moneda')->where('mon_est',1)->get();
        tenancy()->end();
        return $moneda;
    }
    public function store(Request $request)
    {
        $i = $request->json('inquilino');
        $tenancy = $this->Dominio($i);
        $moneda = DB::Table('moneda')->insert([
            'mon_des' =>$request->mon_des,
            'mon_simb'=>$request->mon_simb,
            'mon_est' => 1,
        ]);
        tenancy()->end();
        return $moneda;
    }
    public function show(string $id)
    {
        $moneda = DB::Table('moneda')->where('id_mon',$id)->get();
        return $moneda;
    }
    public function update(Request $request, string $id)
    {
        $i = $request->json('inquilino');
        $tenancy = $this->Dominio($i);

        $moneda = DB::Table('moneda')->where('id_mon',$id)->update([
            'mon_des' =>$request->mon_des,
            'mon_simb'=>$request->mon_simb,
        ]);
        return response()->json(['message' => 'Actualizado correctamente'], 200);
    }
    public function destroy(Request $request, string $id)
    {
        $i = $request->json('inquilino');
        $tenancy = $this->Dominio($i);
        $moneda = DB::Table('moneda')->where('id_mon',$id)->update([
            'mon_est'=>0,
        ]);
        return response()->json(['message' => 'Eliminado correctamente'], 200);
    }
    // jornada
    public function indexJornada()
    {
        $jornada = DB::Table('jornada_laboral')->where('jorlab_est',1)->get();
        return $jornada;
    }
    public function storeJornada(Request $request)
    {
        $jornada = DB::Table('jornada_laboral')->insert([
            'jorlab_nom'=>$request->nombre,
            'jorlab_est'=>1,
        ]);
        return response()->json(['message' => $jornada]);
    }
    public function updateJornada(Request $request, string $id){
        $jornada = DB::Table('jornada_laboral')->where('jorlab_id',$id)->update([
            'jorlab_nom'=>$request->nombre,
        ]);
        return $jornada;
    }
    public function showJornada(string $id)
    {
        $jornada = DB::Table('jornada_laboral')
        ->where('jorlab_id',$id)
        ->where('jorlab_est',1)
        ->first();
        return $jornada;
    }
    public function destroyJornada(string $id)
    {
        $jornada = DB::Table('jornada_laboral')->where('jorlab_id',$id)->update([
            'jorlab_est'=>0,
        ]);
        return $jornada;
    }

}
