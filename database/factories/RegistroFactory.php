<?php

namespace Database\Factories;

use App\Models\Registro;
use Illuminate\Database\Eloquent\Factories\Factory;
use Illuminate\Support\Str;
use Illuminate\Support\Facades\Hash;
class RegistroFactory extends Factory
{
    protected static ?string $password;
    protected $model = Registro::class;

    public function definition(): array
    {
        return [
            'nombres_apellidos' => $this->faker->name,
            'razon_social' => $this->faker->company,
            'gmail' => $this->faker->unique()->safeEmail,
            'telefono' => $this->faker->unique()->phoneNumber,
            'ruc' => $this->faker->unique()->numerify('##########'),
            'password' => static::$password ??= Hash::make('password'), // Puedes ajustar la contraseña según tus necesidades
            'created_at' => now(),
            'updated_at' => now(),
        ];
    }
}
