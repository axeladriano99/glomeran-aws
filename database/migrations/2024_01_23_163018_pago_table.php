<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('cuenta_banco', function (Blueprint $table) {
            $table->bigIncrements('ctaban_id');
            $table->unsignedBigInteger('emp_id');
            $table->foreign('emp_id')->references('idEmpresa')->on('empresa');
            $table->string('ctaban_cod', 10);
            $table->string('tipcta_id', 10);
            $table->string('ctaban_num', 50);
            $table->string('ban_id', 10);
            $table->char('mnd_id', 1);
            $table->string('cta_id', 10);
            $table->boolean('ctaban_est');
            $table->timestamps();
        });
        Schema::create('tipo_caja', function (Blueprint $table) {
            $table->bigIncrements('tcja_id');
            $table->unsignedBigInteger('emp_id');
            $table->foreign('emp_id')->references('idEmpresa')->on('empresa');
            $table->string('tcja_nom', 100);
            $table->char('mnd_id', 1);
            $table->decimal('tcja_mon', 9, 2);
            $table->char('per_id', 6);
            $table->foreign('per_id')->references('per_id')->on('periodo');
            $table->char('cta_ingdh', 1);
            $table->string('cta_ing', 10);
            $table->char('cta_egrdh', 1);
            $table->string('cta_egr', 10);
            $table->bigInteger('cja_id');
            $table->foreign('cja_id')->references('cja_id')->on('caja_tipo');
            $table->boolean('tcja_est');
            $table->timestamps();
        });

        Schema::create('pago', function (Blueprint $table) {
            $table->bigIncrements('pag_id');
            $table->unsignedBigInteger('emp_id');
            $table->foreign('emp_id')->references('idEmpresa')->on('empresa');
            $table->unsignedBigInteger('to_id');
            $table->foreign('to_id')->references('to_id')->on('tipo_operacion');
            $table->string('td_id');
            $table->foreign('td_id')->references('id_doc')->on('tipo_documento');
            $table->char('pag_ser', 4);
            $table->bigInteger('pag_num');
            $table->char('pag_tip', 1);
            $table->char('mov_cja', 2);
            $table->date('pag_fecpag');
            $table->datetime('pag_fecreg');
            $table->char('pag_tipane', 1);
            $table->unsignedBigInteger('ane_id');
            $table->foreign('ane_id')->references('ane_id')->on('anexo');
            $table->char('per_id',6);
            $table->foreign('per_id')->references('per_id')->on('periodo');
            $table->decimal('pag_com', 9, 2);
            $table->decimal('pag_tot', 9, 2);
            $table->string('tippag_id', 10);
            $table->string('ban_id', 10);
            $table->unsignedBigInteger('ctaban_id');
            $table->foreign('ctaban_id')->references('ctaban_id')->on('cuenta_banco');
            $table->string('pag_numope', 50);
            $table->unsignedBigInteger('tcja_id');
            $table->foreign('tcja_id')->references('tcja_id')->on('tipo_caja');
            $table->decimal('pag_tc', 9, 2);
            $table->unsignedBigInteger('cc_id');
            $table->foreign('cc_id')->references('cc_id')->on('centro_costo');
            $table->bigInteger('cpto_id');
            $table->string('pag_obs', 100);
            $table->bigInteger('usu_id');
            $table->boolean('pag_ant')->nullable();
            $table->integer('pag_est');
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        //
    }
};
