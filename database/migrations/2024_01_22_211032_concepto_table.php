<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('concepto', function (Blueprint $table) {
            $table->bigIncrements('cpto_id'); // asumiendo que 'cpto_id' es la clave primaria
            $table->unsignedBigInteger('emp_id');
            $table->foreign('emp_id')->references('idEmpresa')->on('empresa');
            $table->char('mov_cja', 2);
            $table->string('cpto_nom', 50);
            $table->decimal('cpto_mon', 9, 2);
            $table->char('cta_dh', 1)->nullable();
            $table->string('cta_id', 10)->nullable();
            $table->boolean('cpto_est');
            $table->timestamps();

            // Agregar más restricciones de clave foránea según sea necesario
        });

        Schema::create('concepto_plame', function (Blueprint $table) {
            $table->bigIncrements('conpla_id');
            $table->string('conpla_nom');
            $table->integer('conpla_est');
            // Agregar más restricciones de clave foránea según sea necesario
        });
        Schema::create('tipo_contrato', function (Blueprint $table) {
            $table->bigIncrements('tipcon_id');
            $table->string('tipcon_nom');
            $table->integer('tipcon_est');
            // Agregar más restricciones de clave foránea según sea necesario
        });
        Schema::create('tipo_horario', function (Blueprint $table) {
            $table->bigIncrements('tiphor_id');
            $table->string('tiphor_nom');
            $table->integer('tipcon_est');
            $table->date('tiphor_horini');
            $table->date('tiphor_horfin');
            $table->integer('tiphor_est');
        });
        Schema::create('sistema_pension', function (Blueprint $table) {
            $table->bigIncrements('sispen_id');
            $table->string('sispen_nom', 50);
            $table->decimal('sispen_comsobflu', 5, 2);
            $table->decimal('sispen_sobflu', 5, 2);
            $table->decimal('sispen_sobsal', 5, 2);
            $table->decimal('sispen_priseg', 5, 2);
            $table->decimal('sispen_apoobl', 5, 2);
            $table->integer('sispen_tip');
            $table->boolean('sispen_est')->default(true); // Se asume que 'bit' es equivalente a boolean en este contexto
            $table->timestamps();
        });
        Schema::create('sistema_salud', function (Blueprint $table) {
            $table->bigIncrements('sissal_id');
            $table->string('sissal_nom', 50);
            $table->decimal('sissal_por', 5, 2);
            $table->decimal('sissal_imp', 9, 2);
            $table->integer('sissal_tip');
            $table->boolean('sissal_est')->default(true);
            $table->timestamps();
        });

        Schema::create('contrato', function (Blueprint $table) {
            $table->bigIncrements('con_id'); // asumiendo que 'con_id' es la clave primaria
            $table->unsignedBigInteger('emp_id');
            $table->char('per_id',6);
            $table->unsignedBigInteger('car_id');
            $table->unsignedBigInteger('catocu_id')->nullable();
            $table->unsignedBigInteger('tipcon_id');
            $table->date('con_fecini');
            $table->date('con_fecfin');
            $table->date('con_fecing');
            $table->unsignedBigInteger('tiphor_id');
            $table->decimal('con_sue', 9, 2);
            $table->boolean('con_ren5');
            $table->decimal('con_remtot', 9, 2);
            $table->decimal('con_montot', 9, 2);
            $table->unsignedBigInteger('sispen_id');
            $table->string('con_cuspp', 50);
            $table->unsignedBigInteger('sissal_id');
            $table->string('con_otrapo', 50);
            $table->boolean('con_est');
            $table->timestamps();
            $table->foreign('emp_id')->references('idEmpresa')->on('empresa');
            $table->foreign('per_id')->references('per_id')->on('periodo');
            $table->foreign('car_id')->references('car_id')->on('cargo');
            $table->foreign('tipcon_id')->references('tipcon_id')->on('tipo_contrato');
            $table->foreign('tiphor_id')->references('tiphor_id')->on('tipo_horario');
            $table->foreign('sispen_id')->references('sispen_id')->on('sistema_pension');
            $table->foreign('sissal_id')->references('sissal_id')->on('sistema_salud');
        });

    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('concepto');
    }
};
