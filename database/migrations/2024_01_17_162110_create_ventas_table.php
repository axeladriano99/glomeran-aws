<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;
use Illuminate\Support\Facades\DB;
return new class extends Migration
{
    public function up(): void
    {
        Schema::create('condicion_pago', function (Blueprint $table) {
            $table->bigIncrements('id_pago');
            $table->string('pago_nom');
            $table->integer('diacre');
            $table->integer('pago_estado')->default(1);
        });

        Schema::create('lista_precio', function (Blueprint $table) {
            $table->bigIncrements('lp_id');
            $table->bigInteger('emp_id');
            $table->bigInteger('ofi_id');
            $table->string('lp_cod', 10);
            $table->string('lp_nom', 50);
            $table->boolean('lp_est');
        });

        Schema::create('anexo_sucursal', function (Blueprint $table) {
            $table->unsignedBigInteger('ane_id');
            $table->bigIncrements('suc_id');
            $table->char('suc_tipdoc', 1);
            $table->string('suc_numdoc', 20);
            $table->string('suc_nom', 50);
            $table->string('suc_dir', 50);
            $table->char('ubi_id', 6);
            $table->string('suc_dirref', 50);
            $table->string('suc_tel', 50);
            $table->string('suc_ema', 50);
            $table->string('suc_lat', 50);
            $table->string('suc_lon', 50);
            $table->decimal('suc_limcre', 9, 4);
            $table->boolean('suc_est');
            $table->timestamps();
            $table->foreign('ane_id')->references('ane_id')->on('anexo');
        });
        Schema::create('periodo', function (Blueprint $table) {
            $table->char('per_id', 6)->primary();
            $table->unsignedBigInteger('emp_id');
            $table->char('per_ano', 4);
            $table->char('per_mes', 2);
            $table->boolean('per_est');
            $table->timestamps();

            // Definir clave foránea
            $table->foreign('emp_id')->references('idEmpresa')->on('empresa');
        });
        Schema::create('tipo_operacion', function (Blueprint $table) {
            $table->bigIncrements('to_id');
            $table->string('to_nom', 50);
            $table->string('to_frm', 50);
            $table->char('to_tipsuj', 1);
            $table->integer('to_tab');
            $table->string('to_sql', 500);
            $table->string('to_bsql', 500);
            $table->string('to_psql', 500);
            $table->boolean('to_asi');
            $table->boolean('to_kar');
            $table->integer('to_fac');
            $table->boolean('to_est');
            $table->timestamps(); // Agrega los campos created_at y updated_at
        });
        DB::table('tipo_operacion')->insert([
            [
                'to_id' => 1,
                'to_nom' => 'Ventas de Mercaderias',
                'to_frm' => 'venta',
                'to_tipsuj' => 'C',
                'to_tab' => 1,
                'to_sql' => '',
                'to_bsql' => '',
                'to_psql' => '',
                'to_asi' => 1,
                'to_kar' => 1,
                'to_fac' => -1,
                'to_est' => 1,
            ],
            [
                'to_id' => 2,
                'to_nom' => 'Compras de Mercaderias',
                'to_frm' => 'compra',
                'to_tipsuj' => 'P',
                'to_tab' => 2,
                'to_sql' => '',
                'to_bsql' => '',
                'to_psql' => '',
                'to_asi' => 1,
                'to_kar' => 1,
                'to_fac' => 1,
                'to_est' => 1,
            ],
            [
                'to_id' => 3,
                'to_nom' => 'Compras de Honorarios',
                'to_frm' => 'compra_honorario',
                'to_tipsuj' => 'P',
                'to_tab' => 3,
                'to_sql' => '',
                'to_bsql' => '',
                'to_psql' => '',
                'to_asi' => 1,
                'to_kar' => 0,
                'to_fac' => 0,
                'to_est' => 1,
            ],
            [
                'to_id' => 4,
                'to_nom' => 'Compras de servicios',
                'to_frm' => 'compra_servicio',
                'to_tipsuj' => 'P',
                'to_tab' => 4,
                'to_sql' => '',
                'to_bsql' => '',
                'to_psql' => '',
                'to_asi' => 0,
                'to_kar' => 0,
                'to_fac' => 0,
                'to_est' => 1,
            ],
            [
                'to_id' => 5,
                'to_nom' => 'Ventas de servicios',
                'to_frm' => 'venta_servicio',
                'to_tipsuj' => 'C',
                'to_tab' => 5,
                'to_sql' => '',
                'to_bsql' => '',
                'to_psql' => '',
                'to_asi' => 0,
                'to_kar' => 0,
                'to_fac' => 0,
                'to_est' => 1,
            ],
            [
                'to_id' => 6,
                'to_nom' => 'Compra de Materia Prima',
                'to_frm' => 'compra',
                'to_tipsuj' => 'P',
                'to_tab' => 6,
                'to_sql' => '',
                'to_bsql' => '',
                'to_psql' => '',
                'to_asi' => 0,
                'to_kar' => 1,
                'to_fac' => 1,
                'to_est' => 1,
            ],
            [
                'to_id' => 7,
                'to_nom' => 'Compras de Materiales',
                'to_frm' => 'compra',
                'to_tipsuj' => 'P',
                'to_tab' => 7,
                'to_sql' => '',
                'to_bsql' => '',
                'to_psql' => '',
                'to_asi' => 0,
                'to_kar' => 1,
                'to_fac' => 1,
                'to_est' => 1,
            ],
            [
                'to_id' => 8,
                'to_nom' => 'Compras de Gastos',
                'to_frm' => 'compra',
                'to_tipsuj' => 'P',
                'to_tab' => 8,
                'to_sql' => '',
                'to_bsql' => '',
                'to_psql' => '',
                'to_asi' => 0,
                'to_kar' => 0,
                'to_fac' => 0,
                'to_est' => 1,
            ],
            [
                'to_id' => 9,
                'to_nom' => 'Notas de Credito Ventas',
                'to_frm' => 'venta',
                'to_tipsuj' => 'C',
                'to_tab' => 9,
                'to_sql' => '',
                'to_bsql' => '',
                'to_psql' => '',
                'to_asi' => 1,
                'to_kar' => 1,
                'to_fac' => 1,
                'to_est' => 1,
            ],
            [
                'to_id' => 10,
                'to_nom' => 'Notas de Credito Compras',
                'to_frm' => 'compra',
                'to_tipsuj' => 'P',
                'to_tab' => 10,
                'to_sql' => '',
                'to_bsql' => '',
                'to_psql' => '',
                'to_asi' => 0,
                'to_kar' => 1,
                'to_fac' => -1,
                'to_est' => 1,
            ],
            [
                'to_id' => 11,
                'to_nom' => 'Cotizacion',
                'to_frm' => 'venta',
                'to_tipsuj' => 'C',
                'to_tab' => 11,
                'to_sql' => '',
                'to_bsql' => '',
                'to_psql' => '',
                'to_asi' => 0,
                'to_kar' => 0,
                'to_fac' => 0,
                'to_est' => 1,
            ],
            [
                'to_id' => 12,
                'to_nom' => 'Notas de Venta',
                'to_frm' => 'venta',
                'to_tipsuj' => 'C',
                'to_tab' => 12,
                'to_sql' => '',
                'to_bsql' => '',
                'to_psql' => '',
                'to_asi' => 0,
                'to_kar' => 1,
                'to_fac' => -1,
                'to_est' => 1,
            ],
            [
                'to_id' => 13,
                'to_nom' => 'Notas de Remision',
                'to_frm' => 'guia',
                'to_tipsuj' => 'C',
                'to_tab' => 13,
                'to_sql' => '',
                'to_bsql' => '',
                'to_psql' => '',
                'to_asi' => 0,
                'to_kar' => 0,
                'to_fac' => 0,
                'to_est' => 1,
            ],
            [
                'to_id' => 14,
                'to_nom' => 'Ventas de Producto terminado',
                'to_frm' => 'ptovtarest',
                'to_tipsuj' => 'C',
                'to_tab' => 14,
                'to_sql' => '',
                'to_bsql' => '',
                'to_psql' => '',
                'to_asi' => 0,
                'to_kar' => 1,
                'to_fac' => -1,
                'to_est' => 1,
            ],
            [
                'to_id' => 15,
                'to_nom' => 'Pagos',
                'to_frm' => 'pagos',
                'to_tipsuj' => 'P',
                'to_tab' => 15,
                'to_sql' => '',
                'to_bsql' => '',
                'to_psql' => '',
                'to_asi' => 1,
                'to_kar' => 0,
                'to_fac' => 0,
                'to_est' => 1,
            ],
            [
                'to_id' => 16,
                'to_nom' => 'Cobros',
                'to_frm' => 'pagos',
                'to_tipsuj' => 'C',
                'to_tab' => 16,
                'to_sql' => '',
                'to_bsql' => '',
                'to_psql' => '',
                'to_asi' => 1,
                'to_kar' => 0,
                'to_fac' => 0,
                'to_est' => 1,
            ],
            [
                'to_id' => 17,
                'to_nom' => 'Detraccion Pago',
                'to_frm' => 'pagos_dt',
                'to_tipsuj' => 'P',
                'to_tab' => 17,
                'to_sql' => '',
                'to_bsql' => '',
                'to_psql' => '',
                'to_asi' => 1,
                'to_kar' => 0,
                'to_fac' => 0,
                'to_est' => 1,
            ],
            [
                'to_id' => 18,
                'to_nom' => 'Detraccion Cobro',
                'to_frm' => '',
                'to_tipsuj' => 'C',
                'to_tab' => 18,
                'to_sql' => '',
                'to_bsql' => '',
                'to_psql' => '',
                'to_asi' => 1,
                'to_kar' => 0,
                'to_fac' => 0,
                'to_est' => 1,
            ],
            [
                'to_id' => 19,
                'to_nom' => 'Pagos Caja Transferencia',
                'to_frm' => '',
                'to_tipsuj' => 'T',
                'to_tab' => 19,
                'to_sql' => '',
                'to_bsql' => '',
                'to_psql' => '',
                'to_asi' => 1,
                'to_kar' => 0,
                'to_fac' => 0,
                'to_est' => 1,
            ],
            [
                'to_id' => 20,
                'to_nom' => 'Pagos Caja Proveedor',
                'to_frm' => '',
                'to_tipsuj' => 'P',
                'to_tab' => 20,
                'to_sql' => '',
                'to_bsql' => '',
                'to_psql' => '',
                'to_asi' => 1,
                'to_kar' => 0,
                'to_fac' => 0,
                'to_est' => 1,
            ],
            [
                'to_id' => 21,
                'to_nom' => 'Pagos Caja Trabajador',
                'to_frm' => '',
                'to_tipsuj' => 'T',
                'to_tab' => 21,
                'to_sql' => '',
                'to_bsql' => '',
                'to_psql' => '',
                'to_asi' => 1,
                'to_kar' => 0,
                'to_fac' => 0,
                'to_est' => 1,
            ],
            [
                'to_id' => 22,
                'to_nom' => 'Nota de Salida de Materia Prima',
                'to_frm' => 'nota',
                'to_tipsuj' => 'T',
                'to_tab' => 22,
                'to_sql' => '',
                'to_bsql' => '',
                'to_psql' => '',
                'to_asi' => 1,
                'to_kar' => 1,
                'to_fac' => -1,
                'to_est' => 1,
            ],
            [
                'to_id' => 23,
                'to_nom' => 'Nota de Salida de Materiales',
                'to_frm' => 'nota',
                'to_tipsuj' => 'T',
                'to_tab' => 23,
                'to_sql' => '',
                'to_bsql' => '',
                'to_psql' => '',
                'to_asi' => 1,
                'to_kar' => 0,
                'to_fac' => 0,
                'to_est' => 1,
            ],
            [
                'to_id' => 24,
                'to_nom' => 'Nota de Salida de Productos en Proceso',
                'to_frm' => 'nota',
                'to_tipsuj' => 'T',
                'to_tab' => 24,
                'to_sql' => '',
                'to_bsql' => '',
                'to_psql' => '',
                'to_asi' => 1,
                'to_kar' => 1,
                'to_fac' => 0,
                'to_est' => 1,
            ],
            [
                'to_id' => 25,
                'to_nom' => 'Nota de Salida de Productos en Terminados ',
                'to_frm' => 'nota',
                'to_tipsuj' => 'T',
                'to_tab' => 25,
                'to_sql' => '',
                'to_bsql' => '',
                'to_psql' => '',
                'to_asi' => 0,
                'to_kar' => 0,
                'to_fac' => 0,
                'to_est' => 1,
            ],
            [
                'to_id' => 26,
                'to_nom' => 'Nota de Salida de Productos en Proceso',
                'to_frm' => 'nota',
                'to_tipsuj' => 'T',
                'to_tab' => 26,
                'to_sql' => '',
                'to_bsql' => '',
                'to_psql' => '',
                'to_asi' => 1,
                'to_kar' => 1,
                'to_fac' => 1,
                'to_est' => 1,
            ],
            [
                'to_id' => 27,
                'to_nom' => 'Nota de Entrada de Productos en Terminados',
                'to_frm' => 'nota',
                'to_tipsuj' => 'T',
                'to_tab' => 27,
                'to_sql' => '',
                'to_bsql' => '',
                'to_psql' => '',
                'to_asi' => 1,
                'to_kar' => 1,
                'to_fac' => 1,
                'to_est' => 1,
            ],
            [
                'to_id' => 28,
                'to_nom' => 'Canje Letras por Cobrar',
                'to_frm' => 'pagos_lt',
                'to_tipsuj' => 'C',
                'to_tab' => 28,
                'to_sql' => '',
                'to_bsql' => '',
                'to_psql' => '',
                'to_asi' => 1,
                'to_kar' => 0,
                'to_fac' => 0,
                'to_est' => 1,
            ],
            [
                'to_id' => 29,
                'to_nom' => 'Canje Letras por Pagar',
                'to_frm' => 'pagos_lt',
                'to_tipsuj' => 'P',
                'to_tab' => 29,
                'to_sql' => '',
                'to_bsql' => '',
                'to_psql' => '',
                'to_asi' => 1,
                'to_kar' => 0,
                'to_fac' => 0,
                'to_est' => 1,
            ],
            [
                'to_id' => 30,
                'to_nom' => 'Cobrar Letras',
                'to_frm' => 'pagos',
                'to_tipsuj' => 'C',
                'to_tab' => 30,
                'to_sql' => '',
                'to_bsql' => '',
                'to_psql' => '',
                'to_asi' => 1,
                'to_kar' => 0,
                'to_fac' => 0,
                'to_est' => 1,
            ],
            [
                'to_id' => 31,
                'to_nom' => 'Pagar Letras',
                'to_frm' => 'pagos',
                'to_tipsuj' => 'P',
                'to_tab' => 31,
                'to_sql' => '',
                'to_bsql' => '',
                'to_psql' => '',
                'to_asi' => 1,
                'to_kar' => 0,
                'to_fac' => 0,
                'to_est' => 1,
            ],
            [
                'to_id' => 32,
                'to_nom' => 'Nota de Salida de Embalajes',
                'to_frm' => 'nota',
                'to_tipsuj' => 'T',
                'to_tab' => 31,
                'to_sql' => '',
                'to_bsql' => '',
                'to_psql' => '',
                'to_asi' => 1,
                'to_kar' => 1,
                'to_fac' => -1,
                'to_est' => 1,
            ],

            [
                'to_id' => 33,
                'to_nom' => 'Compras de Embalajes',
                'to_frm' => 'compra',
                'to_tipsuj' => 'P',
                'to_tab' => 33,
                'to_sql' => '',
                'to_bsql' => '',
                'to_psql' => '',
                'to_asi' => 0,
                'to_kar' => 0,
                'to_fac' => 1,
                'to_est' => 1,
            ],
        ]);



        Schema::create('ventas', function (Blueprint $table) {
            $table->bigIncrements('id_Venta');
            $table->string('idAlmacen');
            $table->foreign('idAlmacen')->references('idAlmacen')->on('almacen');
            $table->unsignedBigInteger('to_id');
            $table->foreign('to_id')->references('to_id')->on('tipo_operacion');
            $table->string('id_tipoDoc');
            $table->foreign('id_tipoDoc')->references('id_doc')->on('tipo_documento');
            $table->unsignedBigInteger('ven_num')->unique();
            $table->string('ven_serie')->unique();
            $table->unsignedBigInteger('ven_docref');
            $table->date('ven_fecdoc');
            $table->date('ven_fecven');
            $table->date('fecha_creacion');
            $table->date('fecha_almac');
            $table->unsignedBigInteger('ane_id');
            $table->foreign('ane_id')->references('ane_id')->on('anexo');
            $table->unsignedBigInteger('suc_id');
            $table->foreign('suc_id')->references('suc_id')->on('anexo_sucursal');
            $table->char('per_id',6);
            $table->foreign('per_id')->references('per_id')->on('periodo');
            $table->bigInteger('ven_afecto');
            $table->double('ven_ina_afecto',8,2);
            $table->double('ven_isc',8,2);
            $table->double('ven_igv', 8, 2);
            $table->double('ven_total_descuent',8, 2);
            $table->double('ven_tot_doc');
            $table->double('ven_per', 8, 2);
            $table->double('ven_totfin',8,2);
            $table->double('ven_pdt',8,2);
            $table->double('ven_dt',8,2);
            $table->string('mnd_id');
            $table->bigInteger('ven_tc');
            $table->unsignedBigInteger('id_pago');
            $table->foreign('id_pago')->references('id_pago')->on('condicion_pago');
            $table->unsignedBigInteger('lp_id');
            $table->foreign('lp_id')->references('lp_id')->on('lista_precio');
            $table->bigInteger('ven_montsnt');
            $table->string('ven_obs');
            $table->bigInteger('idUsuario')->unsigned();
            $table->foreign('idUsuario')->references('id')->on('users');
            $table->bigInteger('ven_ressnt');
            $table->bigInteger('vent_estado')->default(1);


            $table->timestamps();
        });

        Schema::create('detalle_ventas', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedBigInteger('id_Venta');
            $table->foreign('id_Venta')->references('id_Venta')->on('ventas');
            $table->unsignedBigInteger('idProducto');
            $table->foreign('idProducto')->references('idPro')->on('producto');
            $table->decimal('vd_can', 8, 2);
            $table->string('pst_id');
            $table->foreign('pst_id')->references('idPre')->on('presentacion');
            $table->integer('vd_fact')->default(1);
            $table->integer('vd_des');
            $table->integer('vd_bar');
            $table->decimal('vd_com', 8, 4);
            $table->decimal('vd_impdes', 8, 4);
            $table->decimal('vd_implot', 8, 4);
            $table->decimal('vd_igv', 8, 2)->default(18);
            $table->integer('vd_gra')->default(0);
            $table->unsignedBigInteger('lp_id');
            $table->foreign('lp_id')->references('lp_id')->on('lista_precio');
            $table->integer('vd_ina')->default(0);
            $table->decimal('vd_isc', 8, 2);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('ventas');
        Schema::dropIfExists('lista_precio');
        Schema::dropIfExists('condicion_pago');
        Schema::dropIfExists('anexo');
        Schema::dropIfExists('detalle_ventas');
    }
};
