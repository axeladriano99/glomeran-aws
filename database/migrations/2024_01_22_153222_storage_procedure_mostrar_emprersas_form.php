<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;
use Illuminate\Support\Facades\DB;

return new class extends Migration
{
        public function up(): void
    {
            $procedure = "
        CREATE PROCEDURE spMostrarEmpresaForm()
        BEGIN
        SELECT registros.*, domains.domain AS dominio_empresa
        FROM registros
        LEFT JOIN domains ON registros.idregistro = domains.emp ORDER by registros.idregistro;
        END;
    ";

    DB::unprepared($procedure);

    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        $procedure = "DROP PROCEDURE IF EXISTS spMostrarEmpresaForm";

        DB::unprepared($procedure);
    }


};
