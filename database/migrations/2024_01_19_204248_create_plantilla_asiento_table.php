<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('plantilla_asiento', function (Blueprint $table) {
            $table->bigIncrements('plaasi_id');
            $table->unsignedBigInteger('to_id');
            $table->foreign('to_id')->references('to_id')->on('tipo_operacion');
            $table->integer('tipasi_id');
            $table->string('plaasi_nom');
            $table->string('cc_id');
            $table->string('plaasi_glo');
            $table->integer('plaasi_est');
            $table->timestamps();
        });

        Schema::create('plantilla_asiento_prm', function (Blueprint $table) {
            $table->bigIncrements('prm_id');
            $table->string('prm_nom');
            $table->timestamps();
        });


        Schema::create('plantilla_asiento_det', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedBigInteger('plaasi_id');
            $table->foreign('plaasi_id')->references('plaasi_id')->on('plantilla_asiento');
            $table->string('pad_num');
            $table->unsignedBigInteger('to_id');
            $table->foreign('to_id')->references('to_id')->on('tipo_operacion');
            $table->string('cta_id');
            $table->foreign('cta_id')->references('cta_id')->on('plan_cuenta');
            $table->string('pad_dh');
            $table->unsignedBigInteger('prm_id');
            $table->foreign('prm_id')->references('prm_id')->on('plantilla_asiento_prm');
            $table->string('mnd_id');
            $table->unsignedBigInteger('cc_id');
            $table->foreign('cc_id')->references('cc_id')->on('centro_costo');
            $table->string('pad_glo');
            $table->integer('pad_ord');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('plantilla_asiento_det');
        Schema::dropIfExists('centro_costo');
        Schema::dropIfExists('plantilla_asiento_prm');
        Schema::dropIfExists('plantilla_asiento');
    }
};

