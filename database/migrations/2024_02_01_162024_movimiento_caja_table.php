<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {


        Schema::create('movimiento_caja', function (Blueprint $table) {
            $table->bigIncrements('mcja_id');
            $table->string('movi_nomb');
            $table->unsignedBigInteger('tcja_id');
            $table->foreign('tcja_id')->references('tcja_id')->on('tipo_caja');
            $table->bigInteger('tur_id');
            $table->char('per_id', 6); // Corregir aquí: 'char' en lugar de 'chat'
            $table->foreign('per_id')->references('per_id')->on('periodo');
            $table->dateTime('mcja_fecini'); // Corregir aquí: 'dateTime' en lugar de 'date_time'
            $table->dateTime('mcja_fecfin'); // Corregir aquí: 'dateTime' en lugar de 'date_time'
            $table->decimal('mcja_impini', 9, 2);
            $table->decimal('mcja_impfin', 9, 2);
            $table->unsignedBigInteger('usu_id');
            $table->foreign('usu_id')->references('id')->on('users');
        });

    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        //
    }
};
