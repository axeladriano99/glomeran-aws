<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;
use Illuminate\Support\Facades\DB;
return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('tipo_documento', function (Blueprint $table) {
            $table->string('id_doc')->primary();
            $table->string('nombre');
            $table->integer('td_fac');
            $table->string('td_snt');
            $table->string('td_nombCompleto');
            $table->integer('td_asi');
            $table->integer('td_estado')->default(1);
            $table->timestamps();
        });
        //1
        DB::table('tipo_documento')->insert([
            'id_doc' => 'BO',
            'nombre' => 'Boleta',
            'td_fac' => 1,
            'td_snt' => '03',
            'td_nombCompleto' => 'BOLETA ELECTRONICA',
            'td_asi' => 1,
            'td_estado' => 1,
            'created_at' => now(),
            'updated_at' => now(),
        ]);
        //2
        DB::table('tipo_documento')->insert([
            'id_doc' => 'BR',
            'nombre' => 'Boleta RUS',
            'td_fac' => -1,
            'td_snt' => '00',
            'td_nombCompleto' => 'BOLETA DE VENTA',
            'td_asi' => 1,
            'td_estado' => 1,
            'created_at' => now(),
            'updated_at' => now(),
        ]);
        //3
        DB::table('tipo_documento')->insert([
            'id_doc' => 'CT',
            'nombre' => 'Cotizacion',
            'td_fac' => 0,
            'td_snt' => '00',
            'td_nombCompleto' => 'Cotizacion',
            'td_asi' => 1,
            'td_estado' => 1,
            'created_at' => now(),
            'updated_at' => now(),
        ]);
        //4
        DB::table('tipo_documento')->insert([
            'id_doc' => 'FA',
            'nombre' => 'Factura',
            'td_fac' => 1,
            'td_snt' => '01',
            'td_nombCompleto' => 'Factura Electronica',
            'td_asi' => 1,
            'td_estado' => 1,
            'created_at' => now(),
            'updated_at' => now(),
        ]);
        //5
        DB::table('tipo_documento')->insert([
            'id_doc' => 'GR',
            'nombre' => 'Guia',
            'td_fac' => 0,
            'td_snt' => '09',
            'td_nombCompleto' => 'GUIA DE REMISION',
            'td_asi' => 1,
            'td_estado' => 1,
            'created_at' => now(),
            'updated_at' => now(),
        ]);
        //6
        DB::table('tipo_documento')->insert([
            'id_doc' => 'NC',
            'nombre' => 'Nota de Credito ',
            'td_fac' => -1,
            'td_snt' => '07',
            'td_nombCompleto' => 'NOTA DE CREDITO ELECTRONICA',
            'td_asi' => 1,
            'td_estado' => 1,
            'created_at' => now(),
            'updated_at' => now(),
        ]);
        DB::table('tipo_documento')->insert([
            'id_doc' => 'ND',
            'nombre' => 'Nota de Debito ',
            'td_fac' => 1,
            'td_snt' => '08',
            'td_nombCompleto' => 'NOTA DE DEBITO ELECTRONICA',
            'td_asi' => 1,
            'td_estado' => 1,
            'created_at' => now(),
            'updated_at' => now(),
        ]);
        //7
        DB::table('tipo_documento')->insert([
            'id_doc' => 'NE',
            'nombre' => 'Nota de Entrada ',
            'td_fac' => 0,
            'td_snt' => '00',
            'td_nombCompleto' => 'NOTA DE ENTRADA ELECTRONICA',
            'td_asi' => 0,
            'td_estado' => 1,
            'created_at' => now(),
            'updated_at' => now(),
        ]);
        //8
        DB::table('tipo_documento')->insert([
            'id_doc' => 'NS',
            'nombre' => 'Nota de Salida',
            'td_fac' => 0,
            'td_snt' => '00',
            'td_nombCompleto' => 'NOTA DE SALIDA',
            'td_asi' => 1,
            'td_estado' => 1,
            'created_at' => now(),
            'updated_at' => now(),
        ]);
        //9
        DB::table('tipo_documento')->insert([
            'id_doc' => 'Nv',
            'nombre' => 'Nota de Venta',
            'td_fac' => 1,
            'td_snt' => '00',
            'td_nombCompleto' => 'NOTA DE VENTA',
            'td_asi' => 0,
            'td_estado' => 1,
            'created_at' => now(),
            'updated_at' => now(),
        ]);
        //10
        DB::table('tipo_documento')->insert([
            'id_doc' => 'PF',
            'nombre' => 'Proforma',
            'td_fac' => 0,
            'td_snt' => '00',
            'td_nombCompleto' => 'PROFORMA',
            'td_asi' => 1,
            'td_estado' => 1,
            'created_at' => now(),
            'updated_at' => now(),
        ]);
        //11
        DB::table('tipo_documento')->insert([
            'id_doc' => 'RH',
            'nombre' => 'Recibo por Honorarios',
            'td_fac' => 0,
            'td_snt' => '00',
            'td_nombCompleto' => 'RECIBO POR HONARIOS',
            'td_asi' => 1,
            'td_estado' => 1,
            'created_at' => now(),
            'updated_at' => now(),
        ]);
        //12
        Schema::create('tipo_documento_serie', function (Blueprint $table) {
            $table->string('id_doc');
            $table->foreign('id_doc')->references('id_doc')->on('tipo_documento');
            $table->unsignedBigInteger('idEmpresa');
            $table->foreign('idEmpresa')->references('idEmpresa')->on('empresa');
            $table->string('tds_ser');
            $table->integer('tds_cor');
            $table->integer('tds_est')->default(1);
            $table->timestamps();
        });
    }

    public function down(): void
    {
        Schema::dropIfExists('tipo_documento');
        Schema::dropIfExists('tipo_documento_serie');
    }
};
