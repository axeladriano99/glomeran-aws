<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;
use Illuminate\Support\Facades\DB;
return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        $procedure = "
            CREATE PROCEDURE spUpdateRegistro(
                IN p_id INT,
                IN p_nombres_apellidos VARCHAR(255),
                IN p_gmail VARCHAR(255),
                IN p_telefono VARCHAR(20)
            )
            BEGIN
                UPDATE registros
                SET
                    nombres_apellidos = p_nombres_apellidos,
                    gmail = p_gmail,
                    telefono = p_telefono
                WHERE
                    idregistro = p_id;
            END;
        ";

        $procedures = "
        CREATE PROCEDURE spUpdateAlmacen(
            IN p_id INT,
            IN p_alm_nomb VARCHAR(255),
            IN p_direccion VARCHAR(255)
        )
        BEGIN
            UPDATE almacen
            SET
                alm_nomb = p_alm_nomb,
                direccion = p_direccion 
            WHERE
                idAlmacen = p_id;
        END;
    ";

    DB::unprepared($procedures);

        DB::unprepared($procedure);
    }

    public function down(): void
    {
        $procedure = "DROP PROCEDURE IF EXISTS spUpdateRegistro";
        DB::unprepared($procedure);
    }

};
