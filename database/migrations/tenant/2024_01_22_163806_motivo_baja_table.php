<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;
use Illuminate\Support\Facades\DB;


return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('motivo_baja', function (Blueprint $table) {
            $table->integer('motbaj_id')->primary();
            $table->string('motbaj_nomb');
            $table->char('motest_id');
        });

        $procedure = " INSERT INTO motivo_baja(motbaj_id,motbaj_nomb,motest_id) VALUES (1, 'Renuncia', '1');
        INSERT INTO motivo_baja(motbaj_id,motbaj_nomb,motest_id) VALUES (2, 'Renuncia con Incentivos', '1');
        INSERT INTO motivo_baja(motbaj_id,motbaj_nomb,motest_id) VALUES (3, 'Despido o Destitución', '1');
        INSERT INTO motivo_baja(motbaj_id,motbaj_nomb,motest_id) VALUES (4, 'Cese Colectivo', '1');
        INSERT INTO motivo_baja(motbaj_id,motbaj_nomb,motest_id) VALUES (5, 'Jubilación', '1');
        INSERT INTO motivo_baja(motbaj_id,motbaj_nomb,motest_id) VALUES (6, 'Invalidez Absoluta Permanente', '1');
        INSERT INTO motivo_baja(motbaj_id,motbaj_nomb,motest_id) VALUES (7, 'Termino de Obra/Servicio o Venc. de Plazo', '1');
        INSERT INTO motivo_baja(motbaj_id,motbaj_nomb,motest_id) VALUES (8, 'Mutuo Disenso', '1');
        INSERT INTO motivo_baja(motbaj_id,motbaj_nomb,motest_id) VALUES (9, 'Fallecimiento', '1');
        INSERT INTO motivo_baja(motbaj_id,motbaj_nomb,motest_id) VALUES (10, 'Suspensión de la Pensión', '1');
        INSERT INTO motivo_baja(motbaj_id,motbaj_nomb,motest_id) VALUES (11, 'Reasignación', '1');
        INSERT INTO motivo_baja(motbaj_id,motbaj_nomb,motest_id) VALUES (12, 'Permuta', '1');
        INSERT INTO motivo_baja(motbaj_id,motbaj_nomb,motest_id) VALUES (13, 'Transferencia', '1');
        INSERT INTO motivo_baja(motbaj_id,motbaj_nomb,motest_id) VALUES (14, 'Baja por Suc. En Posicision del Empleador', '1');
        INSERT INTO motivo_baja(motbaj_id,motbaj_nomb,motest_id) VALUES (15, 'Extinsión o Liquidacion del Empleador', '1');
        INSERT INTO motivo_baja(motbaj_id,motbaj_nomb,motest_id) VALUES (16, 'Otro Motivo Caduca Pensión', '1');
        INSERT INTO motivo_baja(motbaj_id,motbaj_nomb,motest_id) VALUES (17, 'No se Inicio la Relación Labolaral o Prest. de Servicio', '1');
        INSERT INTO motivo_baja(motbaj_id,motbaj_nomb,motest_id) VALUES (18, 'Limite de edad 70 años', '1');
        INSERT INTO motivo_baja(motbaj_id,motbaj_nomb,motest_id) VALUES (19, 'Otras Causales - Ley 30057', '1');
        INSERT INTO motivo_baja(motbaj_id,motbaj_nomb,motest_id) VALUES (20, 'Inhab. para ejercer prof. o Función Pub. mas de 3 meses', '1');
        INSERT INTO motivo_baja(motbaj_id,motbaj_nomb,motest_id) VALUES (21, 'Sin Vinculo Laboral', '1');
    ";
    DB::unprepared($procedure);

    }

    public function down(): void
    {
        //
    }
};
