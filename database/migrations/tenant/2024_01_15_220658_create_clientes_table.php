<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;
use Illuminate\Support\Facades\DB;
return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('document', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('doc_nomb');
            $table->timestamps();
        });
        DB::table('document')->insert([
            'doc_nomb' => 'DNI',
        ]);
        DB::table('document')->insert([
            'doc_nomb' => 'RUC',
        ]);
        DB::table('document')->insert([
            'doc_nomb' => 'Carnet de extranjeria',
        ]);
        DB::table('document')->insert([
            'doc_nomb' => 'Pasaporte',
        ]);
        DB::table('document')->insert([
            'doc_nomb' => 'Cedula Diplomatica',
        ]);
        DB::table('document')->insert([
            'doc_nomb' => 'Otros',
        ]);
        Schema::create('anexo', function (Blueprint $table) {
            $table->bigIncrements('ane_id');
            $table->unsignedBigInteger('ane_tipdoc');
            $table->foreign('ane_tipdoc')->references('id')->on('document');
            $table->string('ane_numdoc', 20);
            $table->string('ane_razsoc', 100)->nullable();
            $table->string('ane_nom', 50)->nullable();
            $table->string('ane_apepat', 50)->nullable();
            $table->string('ane_apemat', 50)->nullable();
            $table->string('ane_dir', 100)->nullable();
            $table->string('ubi_id')->nullable();
            $table->string('ane_dirref', 50)->nullable();
            $table->string('ane_tel', 50)->nullable();
            $table->string('ane_ema', 50)->nullable();
            $table->string('ane_lat', 50)->nullable();
            $table->string('ane_lon', 50)->nullable();
            $table->integer('cliente')->nullable();
            $table->boolean('ane_est')->default(1);
            $table->timestamps();
        });

        /*Schema::create('clientes', function (Blueprint $table) {
            $table->integerIncrements('id_cliente');
            $table->string('id_document');
            $table->foreign('id_document')->references('id_document')->on('document');
            $table->string('nombre');
            $table->string('cliente_alias');
            $table->string('direccion');
            $table->string('email');
            $table->integer('telefono');
            $table->integer('telefono_fijo');
            $table->char('id_districts', 6);
            $table->foreign('id_districts')->references('id')->on('districts');
            $table->timestamps();
        });*/
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('clientes');
    }
};
