<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;
use Illuminate\Support\Facades\DB;
return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        //
        $procedure = "
        CREATE PROCEDURE spMostrarAlmacen()
        BEGIN
        SELECT a.*, e.idEmpresa, e.emp_nom
        FROM almacen a
        INNER JOIN empresa e ON a.id_empresa = e.idEmpresa where a.idEstado =1;
        END;

    ";

    DB::unprepared($procedure);
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        //
    }
};
