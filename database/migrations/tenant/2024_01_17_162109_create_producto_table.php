<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;
use Illuminate\Support\Facades\DB;
return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('presentacion', function (Blueprint $table) {
            $table->string('idPre')->primary();
            $table->string('pre_nom');
            $table->string('pst_snt');
            $table->string('pre_estado');
            $table->timestamps();
        });

        $insercion =
        "INSERT INTO presentacion(idPre,pre_nom,pst_snt,pre_estado) VALUES ('AMP', 'AMPOLLA', 'AM', '1');
        INSERT INTO presentacion(idPre,pre_nom,pst_snt,pre_estado) VALUES ('BAL', 'BALDE', 'BJ', '1');
        INSERT INTO presentacion(idPre,pre_nom,pst_snt,pre_estado) VALUES ('BAR', 'BARRILES', 'BLL', '1');
        INSERT INTO presentacion(idPre,pre_nom,pst_snt,pre_estado) VALUES ('BLS', 'BOLSA', 'BG', '1');
        INSERT INTO presentacion(idPre,pre_nom,pst_snt,pre_estado) VALUES ('BOB', 'BOBINAS', '4A', '1');
        INSERT INTO presentacion(idPre,pre_nom,pst_snt,pre_estado) VALUES ('BOT', 'BOTELLAS', 'BO', '1');
        INSERT INTO presentacion(idPre,pre_nom,pst_snt,pre_estado) VALUES ('CAR', 'CARTONES', 'CT', '1');
        INSERT INTO presentacion(idPre,pre_nom,pst_snt,pre_estado) VALUES ('CEN', 'CIENTO DE UNIDADES', 'CEN', '1');
        INSERT INTO presentacion(idPre,pre_nom,pst_snt,pre_estado) VALUES ('CIL', 'CILINDRO', 'CY', '1');
        INSERT INTO presentacion(idPre,pre_nom,pst_snt,pre_estado) VALUES ('CJA', 'CAJA', 'BX', '1');
        INSERT INTO presentacion(idPre,pre_nom,pst_snt,pre_estado) VALUES ('CM', 'CENTIMETRO', 'CMT', '1');
        INSERT INTO presentacion(idPre,pre_nom,pst_snt,pre_estado) VALUES ('CON', 'CONOS', 'CJ', '1');
        INSERT INTO presentacion(idPre,pre_nom,pst_snt,pre_estado) VALUES ('DOC', 'DOCENA', 'DZN', '1');
        INSERT INTO presentacion(idPre,pre_nom,pst_snt,pre_estado) VALUES ('FAR', 'FARDO', 'BE', '1');
        INSERT INTO presentacion(idPre,pre_nom,pst_snt,pre_estado) VALUES ('FRAS', 'FRASCO', 'JR', '1');
        INSERT INTO presentacion(idPre,pre_nom,pst_snt,pre_estado) VALUES ('GAL', 'GALON', 'GLI', '1');
        INSERT INTO presentacion(idPre,pre_nom,pst_snt,pre_estado) VALUES ('GR', 'GRAMO', 'GRM', '1');
        INSERT INTO presentacion(idPre,pre_nom,pst_snt,pre_estado) VALUES ('GRU', 'GRUESA', 'GRO', '1');
        INSERT INTO presentacion(idPre,pre_nom,pst_snt,pre_estado) VALUES ('HECT', 'HECTOLITRO', 'HLT', '1');
        INSERT INTO presentacion(idPre,pre_nom,pst_snt,pre_estado) VALUES ('HOJA', 'HOJA', 'LEF', '1');
        INSERT INTO presentacion(idPre,pre_nom,pst_snt,pre_estado) VALUES ('JUEG', 'JUEGO', 'SET', '1');
        INSERT INTO presentacion(idPre,pre_nom,pst_snt,pre_estado) VALUES ('KG', 'KILOGRAMO', 'KGM', '1');
        INSERT INTO presentacion(idPre,pre_nom,pst_snt,pre_estado) VALUES ('KIT', 'KIT', 'KT', '1');
        INSERT INTO presentacion(idPre,pre_nom,pst_snt,pre_estado) VALUES ('KM', 'KILOMETRO', 'KTM', '1');
        INSERT INTO presentacion(idPre,pre_nom,pst_snt,pre_estado) VALUES ('KV', 'KILOVATIO HORA', 'KWH', '1');
        INSERT INTO presentacion(idPre,pre_nom,pst_snt,pre_estado) VALUES ('LATA', 'LATAS', 'CA', '1');
        INSERT INTO presentacion(idPre,pre_nom,pst_snt,pre_estado) VALUES ('LIB', 'LIBRAS', 'LBR', '1');
        INSERT INTO presentacion(idPre,pre_nom,pst_snt,pre_estado) VALUES ('LT', 'LITRO', 'LTR', '1');
        INSERT INTO presentacion(idPre,pre_nom,pst_snt,pre_estado) VALUES ('MGR', 'MILIGRAMOS', 'MGM', '1');
        INSERT INTO presentacion(idPre,pre_nom,pst_snt,pre_estado) VALUES ('MLL', 'MILLARES', 'MLL', '1');
        INSERT INTO presentacion(idPre,pre_nom,pst_snt,pre_estado) VALUES ('MLT', 'MILILITRO', 'MLT', '1');
        INSERT INTO presentacion(idPre,pre_nom,pst_snt,pre_estado) VALUES ('MMT', 'MILIMETRO', 'MMT', '1');
        INSERT INTO presentacion(idPre,pre_nom,pst_snt,pre_estado) VALUES ('MT', 'METRO', 'MTR', '1');
        INSERT INTO presentacion(idPre,pre_nom,pst_snt,pre_estado) VALUES ('MW', 'MEGAWATT HORA', 'MWH', '1');
        INSERT INTO presentacion(idPre,pre_nom,pst_snt,pre_estado) VALUES ('ONZ', 'ONZAS', 'ONZ', '1');
        INSERT INTO presentacion(idPre,pre_nom,pst_snt,pre_estado) VALUES ('PALT', 'PALETAS', 'PF', '1');
        INSERT INTO presentacion(idPre,pre_nom,pst_snt,pre_estado) VALUES ('PAQ', 'PAQUETE', 'PK', '1');
        INSERT INTO presentacion(idPre,pre_nom,pst_snt,pre_estado) VALUES ('PAR', 'PAR', 'PR', '1');
        INSERT INTO presentacion(idPre,pre_nom,pst_snt,pre_estado) VALUES ('PIE', 'PIES', 'FOT', '1');
        INSERT INTO presentacion(idPre,pre_nom,pst_snt,pre_estado) VALUES ('PIE2', 'PIES CUADRADOS', 'FTK', '1');
        INSERT INTO presentacion(idPre,pre_nom,pst_snt,pre_estado) VALUES ('PIE3', 'PIES CUBICOS', 'FTQ', '1');
        INSERT INTO presentacion(idPre,pre_nom,pst_snt,pre_estado) VALUES ('PLAC', 'PLACAS', 'PG', '1');
        INSERT INTO presentacion(idPre,pre_nom,pst_snt,pre_estado) VALUES ('PLIE', 'PLIEGO', 'ST', '1');
        INSERT INTO presentacion(idPre,pre_nom,pst_snt,pre_estado) VALUES ('PUL', 'PULGADAS', 'INH', '1');
        INSERT INTO presentacion(idPre,pre_nom,pst_snt,pre_estado) VALUES ('PZA', 'PIEZAS', 'C62', '1');
        INSERT INTO presentacion(idPre,pre_nom,pst_snt,pre_estado) VALUES ('TNL', 'TONELADAS', 'TNE', '1');
        INSERT INTO presentacion(idPre,pre_nom,pst_snt,pre_estado) VALUES ('TUB', 'TUBOS', 'TU', '1');
        INSERT INTO presentacion(idPre,pre_nom,pst_snt,pre_estado) VALUES ('UM', 'MILLON DE UNIDADES', 'UM', '1');
        INSERT INTO presentacion(idPre,pre_nom,pst_snt,pre_estado) VALUES ('UND', 'UNIDAD', 'NIU', '1');
        INSERT INTO presentacion(idPre,pre_nom,pst_snt,pre_estado) VALUES ('UNS', 'UNIDAD (SERVICIOS)', 'ZZ', '1');
        INSERT INTO presentacion(idPre,pre_nom,pst_snt,pre_estado) VALUES ('VIAL', 'VIAL LLENO DE POLVO', 'AW', '1');
        INSERT INTO presentacion(idPre,pre_nom,pst_snt,pre_estado) VALUES ('YRD', 'YARDA', 'YRD', '1');";
         DB::unprepared($insercion);
        Schema::create('marca', function (Blueprint $table) {
            $table->bigIncrements('idmarca');
            $table->string('marca_nombre');
            $table->string('marca_estado');
            $table->timestamps();
        });

        Schema::create('categoria', function (Blueprint $table) {
            $table->bigIncrements('id_categoria');
            $table->string('codigo_categoria');
            $table->string('nombre');
            $table->string('categ_estado');
            $table->timestamps();
        });

        Schema::create('subcategoria', function (Blueprint $table) {
            $table->bigIncrements('id_Subcateg');
            $table->string('codigo_subcateg');
            $table->unsignedBigInteger('id_categoria');
            $table->foreign('id_categoria')->references('id_categoria')->on('categoria');
            $table->string('nombre');
            $table->string('sub_abre');
            $table->string('subCat_estado');
            $table->timestamps();
        });

        Schema::create('producto', function (Blueprint $table) {
            $table->bigIncrements('idPro');
            $table->string('idAlmacen');
            $table->foreign('idAlmacen')->references('idAlmacen')->on('almacen');
            $table->string('idPre');
            $table->foreign('idPre')->references('idPre')->on('presentacion');
            $table->integer('pro_fac')->default(1);
            $table->string('pro_nom');
            $table->unsignedBigInteger('pro_catego');
            $table->foreign('pro_catego')->references('id_categoria')->on('categoria');
            $table->unsignedBigInteger('scat_id');
            $table->foreign('scat_id')->references('id_Subcateg')->on('subcategoria');
            $table->decimal('pro_igv', 8, 2);
            $table->decimal('pro_isc', 8, 2);
            $table->integer('pro_est')->default(1);
            $table->integer('pro_ina');
            $table->decimal('pro_sal', 8, 2);
            $table->integer('pro_tip');
            $table->decimal('pro_dh', 8, 2);
            $table->integer('pro_cta');
            $table->decimal('pro_pre', 8, 4);
            $table->integer('pro_bar');
            $table->decimal('pro_pes', 8, 4);
            $table->decimal('pro_codding', 8, 2);
            $table->integer('pro_regsan');
            $table->integer('pro_tir');
            $table->decimal('pro_stock_min', 8, 2);
            $table->decimal('stock_max', 8, 2);
            $table->string('pro_ubi');
            $table->date('pro_fecha_caduci');
            $table->date('pro_fecha_entrada');
            $table->date('pro_fecha_salid');
            $table->decimal('precio', 8, 2);
            $table->double('product_igv', 8, 2);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('producto');
    }
};
