<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('auxiliar', function (Blueprint $table) {
            $table->string('aux_id');
            $table->string('aux_cod');
            $table->string('aux_des');
            $table->integer('aux_est');
        });
        Schema::create('caja_tipo', function (Blueprint $table) {
            $table->bigInteger('cja_id')->primary();
            $table->string('cja_nom');
            $table->integer('aux_est');
        });
        Schema::create('cargo', function (Blueprint $table) {
            $table->bigIncrements('car_id');
            $table->string('car_nom');
            $table->integer('aux_est');
        });
        /*Schema::create('chofer', function (Blueprint $table) {
            $table->id('ch_id');
            $table->unsignedBigInteger('emp_id');
            $table->unsignedBigInteger('ofi_id');
            $table->string('ch_cod', 10);
            $table->unsignedBigInteger('ane_id');
            $table->char('per_id',6);
            $table->char('ch_tipdoc', 1);
            $table->string('ch_numdoc', 15);
            $table->string('ch_razsoc', 100);
            $table->string('ch_nom', 50);
            $table->string('ch_apepat', 50);
            $table->string('ch_apemat', 50);
            $table->string('ch_bre', 50);
            $table->boolean('ch_est');
            $table->timestamps();

            // Foreign key constraints
            $table->foreign('emp_id')->references('idEmpresa')->on('empresa');
            $table->foreign('ane_id')->references('ane_id')->on('anexo');
            $table->foreign('per_id')->references('per_id')->on('periodo');
        });*/
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('auxiliar');
        Schema::dropIfExists('caja_tipo');
        Schema::dropIfExists('cargo');
        Schema::dropIfExists('chofer');
    }
};
