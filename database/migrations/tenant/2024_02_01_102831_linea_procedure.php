<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;
use Illuminate\Support\Facades\DB;
return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        $procedure = "CREATE PROCEDURE spMostrarLinea()
        BEGIN SELECT * FROM linea where lin_est = 1;
        END;";
        $procedures = "CREATE PROCEDURE spMostrarLineaProduccion()
        BEGIN SELECT * FROM linea_produccion where linpro_est = 1;
        END;";
        DB::unprepared($procedure);
        DB::unprepared($procedures);
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        //
    }
};
