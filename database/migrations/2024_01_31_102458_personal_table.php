<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('personal', function (Blueprint $table) {
            $table->bigIncrements('per_id');
            $table->integer('emp_id');
            $table->string('per_cod');
            $table->integer('per_tipdoc');
            $table->bigInteger('per_numdoc');
            $table->string('per_nom');
            $table->string('per_apepat');
            $table->string('per_apemat');
            $table->string('per_dir');
            $table->string('ubi_id');
            $table->string('per_sex');
            $table->string('per_ema');
            $table->date('per_fech');
            $table->integer('per_tele');
            $table->char('per_ven');
            $table->char('per_cho');
            $table->string('are_id');
            $table->string('per_jef');
            $table->string('per_obs');
            $table->integer('per_est')->default(1);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        //
    }
};
