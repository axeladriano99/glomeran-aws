<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;
use Illuminate\Support\Facades\DB;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('concepto_laboral', function (Blueprint $table) {
            $table->string('conlab_cod')->primary();
            $table->string('conlab_nom');
            $table->char('conlab_est');
        });

        $procedure = "INSERT INTO concepto_laboral (conlab_cod,conlab_nom,conlab_est) VALUES ('0100', 'INGRESOS', '0');
        INSERT INTO concepto_laboral (conlab_cod,conlab_nom,conlab_est) VALUES ('0101', 'Alimentación Principal en Dinero', '1');
        INSERT INTO concepto_laboral (conlab_cod,conlab_nom,conlab_est) VALUES ('0102', 'Alimentación Principal en Especie', '1');
        INSERT INTO concepto_laboral (conlab_cod,conlab_nom,conlab_est) VALUES ('0103', 'Comisiones o Destajo', '1');
        INSERT INTO concepto_laboral (conlab_cod,conlab_nom,conlab_est) VALUES ('0104', 'Comisiones Eventuales a Trabajadores', '1');
        INSERT INTO concepto_laboral (conlab_cod,conlab_nom,conlab_est) VALUES ('0105', 'Trabajo en Sobretiempo (Horas Extras) 25%', '1');
        INSERT INTO concepto_laboral (conlab_cod,conlab_nom,conlab_est) VALUES ('0106', 'Trabajo en Sobretiempo (Horas Extras) 35%', '1');
        INSERT INTO concepto_laboral (conlab_cod,conlab_nom,conlab_est) VALUES ('0107', 'Trabajo en día Feriado o día de descanso', '1');
        INSERT INTO concepto_laboral (conlab_cod,conlab_nom,conlab_est) VALUES ('0108', 'Incremento en SNP 3.3%', '1');
        INSERT INTO concepto_laboral (conlab_cod,conlab_nom,conlab_est) VALUES ('0109', 'Incremento por Afiliación a AFP 10.23%', '1');
        INSERT INTO concepto_laboral (conlab_cod,conlab_nom,conlab_est) VALUES ('0110', 'Incremento por Afiliación a AFP 3.00%', '1');
        INSERT INTO concepto_laboral (conlab_cod,conlab_nom,conlab_est) VALUES ('0111', 'Premios por Ventas', '1');
        INSERT INTO concepto_laboral (conlab_cod,conlab_nom,conlab_est) VALUES ('0112', 'Prestaciones Alimentarias - Suministros Directos', '1');
        INSERT INTO concepto_laboral (conlab_cod,conlab_nom,conlab_est) VALUES ('0113', 'Prestaciones Alimentarias - Suministros Indirectos', '1');
        INSERT INTO concepto_laboral (conlab_cod,conlab_nom,conlab_est) VALUES ('0114', 'Vacaciones Truncas', '1');
        INSERT INTO concepto_laboral (conlab_cod,conlab_nom,conlab_est) VALUES ('0115', 'Remuneración día de descanso y feriados', '1');
        INSERT INTO concepto_laboral (conlab_cod,conlab_nom,conlab_est) VALUES ('0116', 'Remuneración en Especie', '1');
        INSERT INTO concepto_laboral (conlab_cod,conlab_nom,conlab_est) VALUES ('0117', 'Compensación Vacacional', '1');
        INSERT INTO concepto_laboral (conlab_cod,conlab_nom,conlab_est) VALUES ('0118', 'Remuneración Vacacional', '1');
        INSERT INTO concepto_laboral (conlab_cod,conlab_nom,conlab_est) VALUES ('0119', 'Remuneraciones Devengadas', '1');
        INSERT INTO concepto_laboral (conlab_cod,conlab_nom,conlab_est) VALUES ('0120', 'Subveción Económica Mensual (Practicante Senati)', '1');
        INSERT INTO concepto_laboral (conlab_cod,conlab_nom,conlab_est) VALUES ('0121', 'Remuneración o Jornal Básico', '1');
        INSERT INTO concepto_laboral (conlab_cod,conlab_nom,conlab_est) VALUES ('0122', 'Remuneración Permanente', '1');
        INSERT INTO concepto_laboral (conlab_cod,conlab_nom,conlab_est) VALUES ('0123', 'Remuneración de los socios de cooperativas', '1');
        INSERT INTO concepto_laboral (conlab_cod,conlab_nom,conlab_est) VALUES ('0124', 'Remuneración por la hora de permiso por lactancia', '1');
        INSERT INTO concepto_laboral (conlab_cod,conlab_nom,conlab_est) VALUES ('0125', 'Remuneración Integral Anual - Cuota', '1');
        INSERT INTO concepto_laboral (conlab_cod,conlab_nom,conlab_est) VALUES ('0126', 'Ingresos del Conductor de la Microempresa Afiliado', '1');
        INSERT INTO concepto_laboral (conlab_cod,conlab_nom,conlab_est) VALUES ('0127', 'Ingresos del Conductor de la Microempresa - Seguro Regular', '1');
        INSERT INTO concepto_laboral (conlab_cod,conlab_nom,conlab_est) VALUES ('0128', 'Remuneración que Excede el Valor de Mercado (Dividendos)', '1');
        INSERT INTO concepto_laboral (conlab_cod,conlab_nom,conlab_est) VALUES ('0200', 'INGRESOS: ASIGNACIONES', '0');
        INSERT INTO concepto_laboral (conlab_cod,conlab_nom,conlab_est) VALUES ('0201', 'Asignación Familiar', '1');
        INSERT INTO concepto_laboral (conlab_cod,conlab_nom,conlab_est) VALUES ('0202', 'Asignación o Bonificación por Educación', '1');
        INSERT INTO concepto_laboral (conlab_cod,conlab_nom,conlab_est) VALUES ('0203', 'Asignación por Cumpleaños', '1');
        INSERT INTO concepto_laboral (conlab_cod,conlab_nom,conlab_est) VALUES ('0204', 'Asignación por Matrimonio', '1');
        INSERT INTO concepto_laboral (conlab_cod,conlab_nom,conlab_est) VALUES ('0205', 'Asignación por Nacimiento de Hijos', '1');
        INSERT INTO concepto_laboral (conlab_cod,conlab_nom,conlab_est) VALUES ('0206', 'Asignación por Fallecimiento de Familiares', '1');
        INSERT INTO concepto_laboral (conlab_cod,conlab_nom,conlab_est) VALUES ('0207', 'Asignación por Otros Motivos Personales', '1');
        INSERT INTO concepto_laboral (conlab_cod,conlab_nom,conlab_est) VALUES ('0208', 'Asignación por Festividad', '1');
        INSERT INTO concepto_laboral (conlab_cod,conlab_nom,conlab_est) VALUES ('0209', 'Asignación Provisional por Demanda de Trabajador (despido)', '1');
        INSERT INTO concepto_laboral (conlab_cod,conlab_nom,conlab_est) VALUES ('0210', 'Asignación Vacacional', '1');
        INSERT INTO concepto_laboral (conlab_cod,conlab_nom,conlab_est) VALUES ('0211', 'Asignación por Escolaridad 30 Jornales Basicos/año', '1');
        INSERT INTO concepto_laboral (conlab_cod,conlab_nom,conlab_est) VALUES ('0212', 'Asignaciones Otorgadas por Unica Vez con Motivo de Cierre', '1');
        INSERT INTO concepto_laboral (conlab_cod,conlab_nom,conlab_est) VALUES ('0213', 'Asignaciones Otorgadas Regularmente', '1');
        INSERT INTO concepto_laboral (conlab_cod,conlab_nom,conlab_est) VALUES ('0214', 'Asignación por Fallecimiento 1 UIT', '1');
        INSERT INTO concepto_laboral (conlab_cod,conlab_nom,conlab_est) VALUES ('0300', 'INGRESOS: BONIFICACIONES', '0');
        INSERT INTO concepto_laboral (conlab_cod,conlab_nom,conlab_est) VALUES ('0301', 'Bonificación por 25 y 30 años de Servicios', '1');
        INSERT INTO concepto_laboral (conlab_cod,conlab_nom,conlab_est) VALUES ('0302', 'Bonificación por Cierre de Pliego', '1');
        INSERT INTO concepto_laboral (conlab_cod,conlab_nom,conlab_est) VALUES ('0303', 'Bonificación por Producción, Altura, Turno, Etc.', '1');
        INSERT INTO concepto_laboral (conlab_cod,conlab_nom,conlab_est) VALUES ('0304', 'Bonificación por Riesgo de Caja', '1');
        INSERT INTO concepto_laboral (conlab_cod,conlab_nom,conlab_est) VALUES ('0305', 'Bonificación por Tiempo de Servicios', '1');
        INSERT INTO concepto_laboral (conlab_cod,conlab_nom,conlab_est) VALUES ('0306', 'Bonificaciones Regulares', '1');
        INSERT INTO concepto_laboral (conlab_cod,conlab_nom,conlab_est) VALUES ('0307', 'Bonificaciones CAFAE', '1');
        INSERT INTO concepto_laboral (conlab_cod,conlab_nom,conlab_est) VALUES ('0308', 'Compensación por Trabajos en Días de Descanso y en Feriados', '1');
        INSERT INTO concepto_laboral (conlab_cod,conlab_nom,conlab_est) VALUES ('0309', 'Bonificación por Turno Nocturno 20% Jornal Basico', '1');
        INSERT INTO concepto_laboral (conlab_cod,conlab_nom,conlab_est) VALUES ('0310', 'Bonificación Contacto Directo con Agua 20% Jornal Básico', '1');
        INSERT INTO concepto_laboral (conlab_cod,conlab_nom,conlab_est) VALUES ('0311', 'Bonificación Unificada de Construcción', '1');
        INSERT INTO concepto_laboral (conlab_cod,conlab_nom,conlab_est) VALUES ('0312', 'Bonificación Extraordinaria Temporal - Leyes 29351', '1');
        INSERT INTO concepto_laboral (conlab_cod,conlab_nom,conlab_est) VALUES ('0313', 'Bonificación Extraordinaria Proporcional - Leyes 29351', '1');
        INSERT INTO concepto_laboral (conlab_cod,conlab_nom,conlab_est) VALUES ('0400', 'INGRESOS: GRATIFICACIONES / AGUINALDOS', '0');
        INSERT INTO concepto_laboral (conlab_cod,conlab_nom,conlab_est) VALUES ('0401', 'Gratificaciones de Fiestas Patrias y Navidad', '1');
        INSERT INTO concepto_laboral (conlab_cod,conlab_nom,conlab_est) VALUES ('0402', 'Otras Gratificaciones Ordinarias', '1');
        INSERT INTO concepto_laboral (conlab_cod,conlab_nom,conlab_est) VALUES ('0403', 'Gratificaciones Extraordinarias', '1');
        INSERT INTO concepto_laboral (conlab_cod,conlab_nom,conlab_est) VALUES ('0404', 'Aguinaldo de Julio y Diciembre', '1');
        INSERT INTO concepto_laboral (conlab_cod,conlab_nom,conlab_est) VALUES ('0405', 'Gratificaciones Proporcional', '1');
        INSERT INTO concepto_laboral (conlab_cod,conlab_nom,conlab_est) VALUES ('0406', 'Gratificaciones de Fiestas Patrias y Navidad - Leyes 29351 Y 30334', '1');
        INSERT INTO concepto_laboral (conlab_cod,conlab_nom,conlab_est) VALUES ('0407', 'Gratificaciones Proporcional - Ley 30334', '1');
        INSERT INTO concepto_laboral (conlab_cod,conlab_nom,conlab_est) VALUES ('0408', 'Gratific Fiestas Patrias y Navidad Trabaj Pesq - Ley 30334', '1');
        INSERT INTO concepto_laboral (conlab_cod,conlab_nom,conlab_est) VALUES ('0409', 'Gratificaciones Proporcional/Trunca Trabaj Pesqueros - Ley 30334', '1');
        INSERT INTO concepto_laboral (conlab_cod,conlab_nom,conlab_est) VALUES ('0410', 'Gratif Forma Parte Remuner Excede Valor de Mercado (DIVIDENDO)', '1');
        INSERT INTO concepto_laboral (conlab_cod,conlab_nom,conlab_est) VALUES ('0500', 'INGRESOS: INDEMNIZACIONES', '0');
        INSERT INTO concepto_laboral (conlab_cod,conlab_nom,conlab_est) VALUES ('0501', 'Indemnización por Despdio Injustificado u Hostilidad', '1');
        INSERT INTO concepto_laboral (conlab_cod,conlab_nom,conlab_est) VALUES ('0502', 'Indemnización por Muerte o Incapacidad', '1');
        INSERT INTO concepto_laboral (conlab_cod,conlab_nom,conlab_est) VALUES ('0503', 'Indemnización por Resolucion de Contrato Sujeto a Modalidad', '1');
        INSERT INTO concepto_laboral (conlab_cod,conlab_nom,conlab_est) VALUES ('0504', 'Indemnización por Vacaciones no Gozadas', '1');
        INSERT INTO concepto_laboral (conlab_cod,conlab_nom,conlab_est) VALUES ('0505', 'Indemnización por Retencion Indebida de CTS ART. 52 D.S Nº 001-97-TR', '1');
        INSERT INTO concepto_laboral (conlab_cod,conlab_nom,conlab_est) VALUES ('0506', 'Indemnización por no Reincorporar a un Trabajador Cesado en un Procedimiento de Cese Colectivo - DS 001-96-TR', '1');
        INSERT INTO concepto_laboral (conlab_cod,conlab_nom,conlab_est) VALUES ('0507', 'Indemnización por Realizar Horas Extras Impuestas por el Empleador', '1');
        INSERT INTO concepto_laboral (conlab_cod,conlab_nom,conlab_est) VALUES ('0700', 'DESCUENTOS AL TRABAJADOR', '0');
        INSERT INTO concepto_laboral (conlab_cod,conlab_nom,conlab_est) VALUES ('0701', 'Adelantado', '1');
        INSERT INTO concepto_laboral (conlab_cod,conlab_nom,conlab_est) VALUES ('0702', 'Cuota Sindical', '1');
        INSERT INTO concepto_laboral (conlab_cod,conlab_nom,conlab_est) VALUES ('0703', 'Descuento Autorizado u Ordenado por Mandato Judicial', '1');
        INSERT INTO concepto_laboral (conlab_cod,conlab_nom,conlab_est) VALUES ('0704', 'Tardanzas', '1');
        INSERT INTO concepto_laboral (conlab_cod,conlab_nom,conlab_est) VALUES ('0705', 'Inasistencias', '1');
        INSERT INTO concepto_laboral (conlab_cod,conlab_nom,conlab_est) VALUES ('0706', 'Otros Descuentos no Deducibles de la Base Imponible', '1');
        INSERT INTO concepto_laboral (conlab_cod,conlab_nom,conlab_est) VALUES ('0707', 'Otros Descuentos Deducibles de la Base Imponible', '1');
        INSERT INTO concepto_laboral (conlab_cod,conlab_nom,conlab_est) VALUES ('0900', 'CONCEPTOS VARIOS', '0');
        INSERT INTO concepto_laboral (conlab_cod,conlab_nom,conlab_est) VALUES ('0901', 'Bienes de la Propia Empresa Otorgada para el Consumo del Trabajador', '1');
        INSERT INTO concepto_laboral (conlab_cod,conlab_nom,conlab_est) VALUES ('0902', 'Bono de Productividad', '1');
        INSERT INTO concepto_laboral (conlab_cod,conlab_nom,conlab_est) VALUES ('0903', 'Canasta de Navidad o Similares', '1');
        INSERT INTO concepto_laboral (conlab_cod,conlab_nom,conlab_est) VALUES ('0904', 'Compensacion por Tiempo de Servicio', '1');
        INSERT INTO concepto_laboral (conlab_cod,conlab_nom,conlab_est) VALUES ('0905', 'Gasto de Representacion (Movilidad, Vestuarios, Viàticos y Similares) - Libre Disponibilidad', '1');
        INSERT INTO concepto_laboral (conlab_cod,conlab_nom,conlab_est) VALUES ('0906', 'Incentivo por Cese del Trabajador', '1');
        INSERT INTO concepto_laboral (conlab_cod,conlab_nom,conlab_est) VALUES ('0907', 'Licencia con Goce de Haber', '1');
        INSERT INTO concepto_laboral (conlab_cod,conlab_nom,conlab_est) VALUES ('0908', 'Movilidad de Libre Disposición', '1');
        INSERT INTO concepto_laboral (conlab_cod,conlab_nom,conlab_est) VALUES ('0909', 'Movilidad Supeditada a Asistencia que Cubre Solo el Traslado', '1');
        INSERT INTO concepto_laboral (conlab_cod,conlab_nom,conlab_est) VALUES ('0910', 'Participacion en las Utilidades - Pagadas Antes de la Declaracion Anual del Impuesto a la Renta', '1');
        INSERT INTO concepto_laboral (conlab_cod,conlab_nom,conlab_est) VALUES ('0911', 'Participacion en las Utilidades - Pagadas Despues de la Declaracion Anual del Impuesto a la Renta', '1');
        INSERT INTO concepto_laboral (conlab_cod,conlab_nom,conlab_est) VALUES ('0912', 'Pensiones de Jubilaciòn o Cesantìa, Montepìo o Invalidez', '1');
        INSERT INTO concepto_laboral (conlab_cod,conlab_nom,conlab_est) VALUES ('0913', 'Recargo al Consumo', '1');
        INSERT INTO concepto_laboral (conlab_cod,conlab_nom,conlab_est) VALUES ('0914', 'Refrigerio que no es Alimentaciòn Principal', '1');
        INSERT INTO concepto_laboral (conlab_cod,conlab_nom,conlab_est) VALUES ('0915', 'Subsidios por Maternidad', '1');
        INSERT INTO concepto_laboral (conlab_cod,conlab_nom,conlab_est) VALUES ('0916', 'Subsidios de Incapacidad por Enfermedad', '1');
        INSERT INTO concepto_laboral (conlab_cod,conlab_nom,conlab_est) VALUES ('0917', 'Condiciones de Trabajo', '1');
        INSERT INTO concepto_laboral (conlab_cod,conlab_nom,conlab_est) VALUES ('0918', 'Impuesto a la Renta de Quinta Categoria Asumido', '1');
        INSERT INTO concepto_laboral (conlab_cod,conlab_nom,conlab_est) VALUES ('0919', 'Sistema Nacional de Pensiones Asumidos', '1');
        INSERT INTO concepto_laboral (conlab_cod,conlab_nom,conlab_est) VALUES ('0920', 'Sistema Privado de Pensiones Asumidos', '1');
        INSERT INTO concepto_laboral (conlab_cod,conlab_nom,conlab_est) VALUES ('0921', 'Pensiones de Jubilaciòn o Cesantìa, Montepìo o Invalidez Pendientes por Liquidar', '1');
        INSERT INTO concepto_laboral (conlab_cod,conlab_nom,conlab_est) VALUES ('0922', 'Sumas o Bienes que no son de Libre Disposiciòn', '1');
        INSERT INTO concepto_laboral (conlab_cod,conlab_nom,conlab_est) VALUES ('0923', 'Ingreso de Cuarta Categoria que son Considerado de Quinta Categoria', '1');
        INSERT INTO concepto_laboral (conlab_cod,conlab_nom,conlab_est) VALUES ('0924', 'Ingreso de Cuarta -  Quinta Categoria sin Relaciòn de Dependencia', '1');
        INSERT INTO concepto_laboral (conlab_cod,conlab_nom,conlab_est) VALUES ('0925', 'Ingreso del Pescador y Procesador Artesanal Independiente - Base de Calculo Aporte Essalud - Ley 27177', '1');
        INSERT INTO concepto_laboral (conlab_cod,conlab_nom,conlab_est) VALUES ('0926', 'Transferencia Directa al Expescador - Ley 30003', '1');
        INSERT INTO concepto_laboral (conlab_cod,conlab_nom,conlab_est) VALUES ('0927', 'Bonificaciòn Prima Textil', '1');
        INSERT INTO concepto_laboral (conlab_cod,conlab_nom,conlab_est) VALUES ('0928', 'Devolución Retención Exceso de Imp Renta 5ta Categoría al Cese o Fin del Vinculo Laboral', '1');
        INSERT INTO concepto_laboral (conlab_cod,conlab_nom,conlab_est) VALUES ('0929', 'Otras Asign Bonif que Forman Parte Remuner y Exced Valor de Mercado (DIVIDENDOS)', '1');
        INSERT INTO concepto_laboral (conlab_cod,conlab_nom,conlab_est) VALUES ('1000', 'OTROS CONCEPTOS', '0');
        INSERT INTO concepto_laboral (conlab_cod,conlab_nom,conlab_est) VALUES ('1001', 'Reintegros no Afectos', '1');
        ";



    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        //
    }
};
