<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;
use Illuminate\Support\Facades\DB;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('codigo_telefono', function (Blueprint $table) {
            $table->string('tel_id');
            $table->string('nombre');
        });
        $procedure ="INSERT INTO codigo_telefono(tel_id,nombre) VALUES ('01', 'Lima y Callao');
        INSERT INTO codigo_telefono(tel_id,nombre) VALUES ('41', 'Amazonas');
        INSERT INTO codigo_telefono(tel_id,nombre) VALUES ('42', 'San Martin');
        INSERT INTO codigo_telefono(tel_id,nombre) VALUES ('43', 'Ancash');
        INSERT INTO codigo_telefono(tel_id,nombre) VALUES ('44', 'La Libertad');
        INSERT INTO codigo_telefono(tel_id,nombre) VALUES ('51', 'Puno');
        INSERT INTO codigo_telefono(tel_id,nombre) VALUES ('52', 'Tacna');
        INSERT INTO codigo_telefono(tel_id,nombre) VALUES ('53', 'Moquegua');
        INSERT INTO codigo_telefono(tel_id,nombre) VALUES ('54', 'Arequipa');
        INSERT INTO codigo_telefono(tel_id,nombre) VALUES ('56', 'Ica');
        INSERT INTO codigo_telefono(tel_id,nombre) VALUES ('61', 'Ucayali');
        INSERT INTO codigo_telefono(tel_id,nombre) VALUES ('62', 'Huanuco');
        INSERT INTO codigo_telefono(tel_id,nombre) VALUES ('63', 'Pasco');
        INSERT INTO codigo_telefono(tel_id,nombre) VALUES ('64', 'Junin');
        INSERT INTO codigo_telefono(tel_id,nombre) VALUES ('65', 'Loreto');
        INSERT INTO codigo_telefono(tel_id,nombre) VALUES ('66', 'Ayacucho');
        INSERT INTO codigo_telefono(tel_id,nombre) VALUES ('67', 'Huancavelica');
        INSERT INTO codigo_telefono(tel_id,nombre) VALUES ('72', 'Tumbes');
        INSERT INTO codigo_telefono(tel_id,nombre) VALUES ('73', 'Piura');
        INSERT INTO codigo_telefono(tel_id,nombre) VALUES ('74', 'Lambayeque');
        INSERT INTO codigo_telefono(tel_id,nombre) VALUES ('76', 'Cajamarca');
        INSERT INTO codigo_telefono(tel_id,nombre) VALUES ('82', 'Madre de Dios');
        INSERT INTO codigo_telefono(tel_id,nombre) VALUES ('83', 'Apurimac');
        INSERT INTO codigo_telefono(tel_id,nombre) VALUES ('84', 'Cusco');
        INSERT INTO codigo_telefono(tel_id,nombre) VALUES ('94', 'La Libertad');
        INSERT INTO codigo_telefono(tel_id,nombre) VALUES ('95', 'Arequipa');
        INSERT INTO codigo_telefono(tel_id,nombre) VALUES ('96', 'Piura');
        INSERT INTO codigo_telefono(tel_id,nombre) VALUES ('97', 'Lambayeque');";
    DB::unprepared($procedure);


    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        //
    }
};
