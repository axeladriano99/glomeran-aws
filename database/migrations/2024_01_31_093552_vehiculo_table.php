<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('chofer', function (Blueprint $table) {
            $table->bigIncrements('ch_id');
            $table->unsignedBigInteger('idEmpresa');
            $table->foreign('idEmpresa')->references('idEmpresa')->on('empresa');
            $table->string('ch_cod');
            $table->unsignedBigInteger('ane_id');// anexo id
            $table->char('per_id',6);// enlazar
            $table->foreign('per_id')->references('per_id')->on('periodo');
            $table->string('id_doc');//tipo de documento
            $table->foreign('id_doc')->references('id_doc')->on('tipo_documento');
            $table->string('ch_numdoc');
            $table->string('ch_razsoc');
            $table->string('ch_nom');
            $table->string('ch_apepat');
            $table->string('ch_bre');
            $table->integer('ch_est');
        });

        Schema::create('vehiculo', function (Blueprint $table) {
            $table->bigIncrements('vh_id');
            $table->unsignedBigInteger('idEmpresa');
            $table->foreign('idEmpresa')->references('idEmpresa')->on('empresa');
            $table->string('vh_cod');
            $table->unsignedBigInteger('ane_id');
            $table->unsignedBigInteger('ch_id');
            $table->foreign('ch_id')->references('ch_id')->on('chofer');
            $table->string('vh_mar');
            $table->string('vh_pla');
            $table->string('vh_des');
            $table->string('vh_mod');
            $table->integer('vh_est');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        //
    }
};
