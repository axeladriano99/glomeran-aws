<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('linea', function (Blueprint $table) {
            $table->bigIncrements('lin_id');
            $table->unsignedBigInteger('idEmpresa');
            $table->foreign('idEmpresa')->references('idEmpresa')->on('empresa')->cascade()->delete();
            $table->string('lin_cod');
            $table->string('lin_nom');
            $table->string('lin_abr');
            $table->integer('lin_est');
        });
        Schema::create('linea_produccion', function (Blueprint $table) {
            $table->bigIncrements('linpro_id');
            $table->string('linpro_nom');
            $table->integer('linpro_est');
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        //
    }
};
